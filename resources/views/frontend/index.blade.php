@extends('frontend.inc')

@section('content')

  <section class="dorne-welcome-area bg-img bg-overlay d-flex justify-content-center"
           style="background-image: url({{asset('img/hero2.jpg')}});">
    <div class="container d-flex align-items-end align-items-md-center justify-content-center welcome-container">
      <div class="row welcome-row">
        <div class="d-flex justify-content-center w-lg-auto w-100">

          <div class="col-md-12 col-lg-12">
            <div class="hero-content">
              <img src="{{asset('img/utazz-a-seffel-logo-png-03.png')}}" alt="utazz a Séffel">
            </div>
            <!-- Hero Search Form -->
            <div class="hero-search-form">
              <!-- Tabs Content -->
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane active" id="nav-places" role="tabpanel"
                     aria-labelledby="nav-places-tab">
                  <form action="{{route('restaurants.search')}}" method="get">
                    <div class="row welcome-row-inner">

                      <div class="hero-search-input d-flex flex-column col-12 col-md-3 ">
                        <label for="hero-search-input-1">Típus</label>
                        <select class="custom-select" name="search[category]" id="hero-search-input-1">

                          
                          <option value="" disabled selected>Kiválasztás</option>
                          @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                          @endforeach
                            
                        </select>
                      </div>
                      <div
                        class="hero-search-input d-flex flex-column col-12 col-md-4 mt-3 mt-md-0">
                        <label for="hero-search-input-2">Megye</label>
                        <select class="custom-select" id="hero-search-input-2" name="search[county]">

                          <option value="" disabled selected>Kiválasztás</option>
                        
                          <option>Bács-Kiskun megye</option>
                          <option>Baranya megye</option>
                          <option>Békés megye</option>
                          <option>Borsod-Abaúj-Zemplén megye</option>
                          <option>Csongrád megye</option>
                          <option>Fejér megye</option>
                          <option>Győr-Moson-Sopron megye</option>
                          <option>Hajdú-Bihar megye</option>
                          <option>Heves megye</option>
                          <option>Jász-Nagykun-Szolnok megye</option>
                          <option>Komárom-Esztergom megye</option>
                          <option>Nógrád megye</option>
                          <option>Pest megye</option>
                          <option>Somogy megye</option>
                          <option>Szabolcs-Szatmár-Bereg megye</option>
                          <option>Tolna megye</option>
                          <option>Vas megye</option>
                          <option>Veszprém megye</option>
                          <option>Zala megye</option>
                          
                        </select>
                      </div>
                      <div
                        class="hero-search-input d-flex flex-column col-12 col-md-2 mt-3 mt-md-0">
                        <label for="hero-search-input-3">Összbenyomás</label>
                        <select class="custom-select" id="hero-search-input-3" name="search[rate]">
                          <option value="" disabled selected>Értékelés</option>
                          <option value="10">10</option>
                          <option value="9">9</option>
                          <option value="8">8</option>
                          <option value="7">7</option>
                          <option value="6">6</option>
                          <option value="5">5</option>
                          <option value="4">4</option>
                          <option value="3">3</option>
                          <option value="2">2</option>
                          <option value="1">1</option>
                          
                        </select>
                      </div>
                      <div
                        class="hero-search-input d-flex flex-column col-12 col-md-3 mt-4 mt-md-0">
                        <button type="submit"
                                class="btn dorne-btn search-hero-dorne-btn mt-auto"><i
                            class="fa fa-search pr-2" aria-hidden="true"></i>
                          Keresés
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Hero Social Btn -->
    <div class="hero-social-btn">
      <div style="display:flex" class="social-title align-items-center">
        <h6>Kövesd közösségi oldalainkat</h6>
        <span></span>
      </div>
      <div class="social-btns">
        <a href="https://www.facebook.com/%C3%89tteremkalauz-utazz-a-s%C3%A9ffel-312768186300922/"><i
            class="fab fa-instagram"></i></a>
        <a href="https://www.facebook.com/%C3%89tteremkalauz-utazz-a-s%C3%A9ffel-312768186300922/"><i
            class="fab fa-facebook-f"></i></a>
      </div>
    </div>
  </section>
  <!-- ***** Welcome Area End ***** -->

  <!-- ***** Catagory Area Start ***** -->
  <section class="dorne-catagory-area">
    <div class="container">
      <div class="row-12 ">
        <div class="col-12">
          <div class="all-catagories">
            <div class="row ">
              <!-- Single Catagory Area -->
              <div class="col-12 col-sm-6 col-md kartyak">
                <a href="{{route('restaurants.byCategories', ['category_slug' => 'nemzetkozi-es-streetfood'])}}" style="display: block;">
                  <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.2s">
                    <div class="catagory-content">
                      <i class="fas fa-hamburger"></i>

                      <h6>Nemzetközi / Street food</h6>

                    </div>
                  </div>
                </a>
              </div>
              <!-- Single Catagory Area -->
              <div class="col-12 col-sm-6 col-md kartyak">
                <a href="{{route('restaurants.byCategories', ['category_slug' => 'modern-fine-dining-ettermek'])}}" style="display: block;">
                  <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.4s">
                    <div class="catagory-content">
                      <i class="fas fa-utensils"></i>

                      <h6>Modern, fine dining</h6>

                    </div>
                  </div>
                </a>
              </div>
              <!-- Single Catagory Area -->
              <div class="col-12 col-sm-6 col-md kartyak">
                <a href="{{route('restaurants.byCategories', ['category_slug' => 'magyaros-ettermek'])}}" style="display: block;">
                  <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.6s">
                    <div class="catagory-content">
                      <i class="fas fa-pepper-hot"></i>

                      <h6>Magyaros éttermek</h6>

                    </div>
                  </div>
                </a>
              </div>

              <!-- Single Catagory Area -->
              <div class="col-12 col-sm-6 col-md kartyak">
                <a href="{{route('restaurants.byCategories', ['category_slug' => 'cukraszda'])}}" style="display: block;">
                  <div class="single-catagory-area wow fadeInUpBig" data-wow-delay="0.8s">
                    <div class="catagory-content">
                      <i class="fas fa-birthday-cake"></i>

                      <h6>Cukrászda</h6>

                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ***** Catagory Area End ***** -->

  <!-- ***** About Area Start ***** -->
  <section class="dorne-about-area section-padding-0-100">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="about-content text-center">
            <h2>Kedves <span>Látogató!</span></h2>
            <p>Ezt az oldalt azért hoztuk létre, hogy segítséget nyújtsunk Neked.</p>
            <br>
            <p>Finomat ennél, jó helyen? Kipróbálnál új helyeket, és biztosra szeretnél menni? Olyan éttermet
              keresel, ami sok szolgáltatást nyújt? (Wifi, gyerekbarát, állatbarát, akadálymentesített...)
            </p>
            <br>
            <p>Mi segítünk! Járjuk az országot, és felkutatjuk Magyarország legjobb éttermeit, cukrászdáit.
              Pontozzuk őket, így utána tudsz nézni, hogy magyaros-, nemzetközi / street food-, modern /
              fine dining éttermek vagy cukrászdák közül melyek azok, amik a legszínvonalasabbak és
              ár-érték arányban a legkedvezőbbek.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ***** About Area End ***** -->

  <!-- ***** Features Restaurant Area Start ***** -->
  <section class="dorne-editors-pick-area bg-img bg-overlay-99 section-padding-100 px-0 px-5 px-lg-3 pt-0">
    <div class="container-fluid">
      <div class="row ">
        <div class="col-12">
          <div class="section-heading section-heading-dark text-center">
            <h4>Hirdetés</h4>
            <span></span>
          </div>
        </div>
        @foreach ($ads as $ad)
          <div class="ad-containers col-sm-12 col-lg-4 col-md-12 col-xs-12 px-0 px-md-5 px-lg-3">
            <a target="_blank" href="{{$ad->url}}">
              <div class="single-editors-pick-area px-0 px-sm-5 px-lg-0 wow fadeInUp " data-wow-delay="0.2s"
                   style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <img src="/storage/restaurants/{{$ad->image}}" style="width:100%; height: 290px; object-fit:cover" class=" d-block" alt="">
                <div class="hirdetes-info d-block">
                  <h3 class="hirdetes-title">{{$ad->title}}
                  </h3>
                  <p class="hirdetes-subtitle">{{$ad->sub_title}} |<span> Hirdetés</span></p>
                </div>
              </div>
            </a>
          </div>
        @endforeach

      </div>
    </div>
  </section>
  <!-- ***** Features Restaurant Area End ***** -->

  <!-- ***** Editor Pick Area Start ***** -->
  <section class="dorne-editors-pick-area bg-img bg-overlay-9 section-padding-100 px-0 px-md-3"
           style="background-image: url({{asset('img/bg-img/hero-2.jpg')}});">
    <div class="container-fluid">
      <div class="row ">
        <div class="col-12">
          <div class="section-heading text-center">
            <span></span>
            <h4>Az ország legjobb éttermei</h4>
            <p>A Séf ajánlata</p>
          </div>
        </div>

        @foreach ($emphasized as $restaurant)
          <div class="col-sm-12 col-lg-4 col-md-6 col-xs-12">
            <div style="width:100%" class="single-editors-pick-area wow fadeInUp " data-wow-delay="0.2s"
                 style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
              <img style="width:100%; height: 290px; object-fit:cover" src="storage/restaurants/{{$restaurant->image}}" class="img-fluid" alt="">
              <div class="editors-pick-info">
                <div class="places-total-destinations d-flex flex-wrap">
                  <a href="{{$restaurant->url}}">{{$restaurant->name}}</a>
                  <a  href="{{$restaurant->url}}">{{$restaurant->city}}</a>
                </div>
                <a  href="{{route('restaurant.show', ['category_slug' => $restaurant->getCategorySlug(), 'restaurant_slug' => $restaurant->slug])}}" class="tovabb">Tovább az
                  étteremhez</a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- ***** Editor Pick Area End ***** -->

  <div class="container-fluid dorne-clients-areax d-flex mb-0">
    <div class="row px-3">
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo2.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo3.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo4.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo5.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
      <div class="col-4 col-md-2 py-2">
        <img src="img/logo6.jpg" class="img-fluid logo" alt="utazz-a-seffel-logo">
      </div>
    </div>
  </div>

@endsection



    