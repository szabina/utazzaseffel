
<form action="
{{$admin->id ? route('admin.update',  ['admin_id' => $admin->id]) : route('admin.store')}}
{{--HA VAN IDJA A $user-nak AZ AZT JELENTI, HOGY MÁR LÉTEZIK, EZÉRT EKKOR AZ UPDATE-RA MEGYÜNK
  HA NINCS, AKKOR MÉG NEMLÉTEZIK, TEHÁT A STORE-RA MEGYÜNK--}}
" method="POST">
  {{csrf_field()}}
  
  <label>Név</label>
  <br>
  <input type="text" name="name" value="{{old('name') ?: $admin->name}}">
  <br>
  @if($errors->first('name'))
    <p >
      {{$errors->first('name')}}
    </p>
  @endif
  <br>
  <label>Email</label>
  <br>
  <input type="text" name="email" value="{{old('email') ?: $admin->email}}">
  <br>
  @if($errors->first('email'))
    <p >
      {{$errors->first('email')}}
    </p>
  @endif
  <br>
  <label>Jelszó</label>
  <br>
  @if($errors->first('password'))
    <p >
      {{$errors->first('password')}}
    </p>
  @endif

  <input type="password" name="password">
  <br><br>
  <label>Jelszó ismét</label>
  <br>
  @if($errors->first('password_confirmation'))
    <p >
      {{$errors->first('password_confirmation')}}
    </p>
  @endif
  <input type="password" name="password_confirmation">
  <br><br>
  @if(auth()->guard('admin')->user()->hasPermission(\App\Users\Models\Permission::EDIT_USERS))
  <label>Szerepkör</label>
  <select name="role_id">
    @foreach($roles as $role)
      <option value="{{$role->id}}"
        {{old('role_id') == $role->id || $admin->role_id == $role->id ? 'selected' : ''}}>{{$role->name}}</option>
    @endforeach
  </select>
  {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}

  @endif
  <br><br>
  <button class="btn btn-success" type="submit">Mentés</button>
</form>
A Superadmin szerepkörrel rendelkező felhasználónak lehetősége van új admin hozzáadására, módosítására, törlésére. Az admin felhasználók az oldal tartalmát szerkeszthetik és az üzeneteket is megkapják.
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif