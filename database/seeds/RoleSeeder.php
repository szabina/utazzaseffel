<?php

use Illuminate\Database\Seeder;
use App\Users\Models\Role;
use App\Users\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->id = Role::SUPERADMIN;
        $role->name = 'Superadmin';
        $role->save();

        $permissionIds = Permission::pluck('id')->toArray();
        $role->permissions()->attach($permissionIds);

        $role_2 = new Role;
        $role_2->name = 'Admin';
        $role_2->save();
        $role_2->permissions()->attach(Permission::EDIT_CONTENT);
    }
}
