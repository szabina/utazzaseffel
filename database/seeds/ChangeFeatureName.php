<?php

use Illuminate\Database\Seeder;
use App\Models\Feature;


class ChangeFeatureName extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Kupon jellemző átnevezése

        $kupon = Feature::find(8);
        $kupon->name = "SZÉP kártya";
        $kupon->save();
    }
}
