<?php

use Illuminate\Database\Seeder;
use App\Users\Models\Role;
use App\Users\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Admin;
        $user->name = 'admin';
        $user->email = 'admin@admin.hu';
        $user->password = \Hash::make('00000000');
        $user->role()->associate(Role::SUPERADMIN);
        $user->save();
    }
}
