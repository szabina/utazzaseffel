<?php

use Illuminate\Database\Seeder;
use App\Models\Category;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nemzetkozi = new Category;
        $nemzetkozi->name = "Nemzetközi / street food";
        $nemzetkozi->description = 'Ebben a kategóriában olyan éttermeket találsz, ahol megismerkedhetsz a nemzetközi konyhával, és magyar ízlés szerint, a legkiválóbb nemzetközi ételekkel. 
        Meglátogatjuk Magyarország street food éttermeit is, amikben "kézzel fogyasztható" ételeket kóstolunk és pontozunk.';
        $nemzetkozi->save();

        $modern = new Category;
        $modern->name = "Modern, fine dining";
        $modern->description = 'Ez a kategória olyan éttermeket tartalmaz, ahol a modern konyhaművészet remekeit kóstolhatjátok meg. Igazán itt érvényesül a séfek kreativitása. Újragondolt ételekben, ízvilágban, látványban lehet részetek és modern elkészítési módokkal, újfajta alapanyagokkal találkozhattok, ha fine dinning étteremben vacsoráztok.';
        $modern->save();

        $magyar = new Category;
        $magyar->name = "Magyaros éttermek";
        $magyar->description = 'Ennél a kategóriánál a hagyományos magyar éttermeket látogatjuk meg, és főként a magyar ízvilágot keressük az ételekben. Megkóstoljuk Magyarország egy-egy tájegységének jellegzetes ételeit, ízeit is.';
        $magyar->save();

        $cukraszda = new Category;
        $cukraszda->name = "Cukrászda";
        $cukraszda->description = 'Magyarország legkiválóbb cukrászdáit igyekszünk ebben a kategóriában bemutatni. Célunk, nem a "tömeggyártott" sütemények kóstolása, hanem a hazai ízek és az újfajta desszertek keresése; kézműves finomságokat készítő, kisebb, hangulatos helyek felkutatása, ahonnan katarzisélménnyel távozik az ember.';
        $cukraszda->save();
    }
}
