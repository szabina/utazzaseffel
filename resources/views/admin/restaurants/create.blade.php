@extends('admin.layouts.template')

@section('content')

<h1>Étterem hozzáadása</h1>
<div class="form-three widget-shadow">
<form class="form-horizontal" action="{{route('restaurant.store')}}" method="POST" enctype="multipart/form-data">
@csrf

@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
@if(Session::has('error-msg'))
<p class="alert alert-danger">{{ Session::get('error-msg') }}</p>
@endif

<br><br>Név *:
<input type="text" name="name" value="{{ old('name') }}">
{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
<br><br>Város *:
<input type="text" name="city" value="{{ old('city') }}">
{!! $errors->first('city', '<p class="help-block">:message</p>') !!}
<br><br>Típus *: 
<select name="category_id">
    @foreach ($categories as $category)
        <option value="{{$category->id}}">{{$category->name}}</option>
    @endforeach
</select>
{!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}

<br><br>Megye *:
<select name="county">
     <option >Bács-Kiskun megye</option>
    <option>Baranya megye</option>
    <option >Békés megye</option>
    <option >Borsod-Abaúj-Zemplén megye</option>
    <option >Csongrád megye</option>
    <option>Fejér megye</option>
    <option >Győr-Moson-Sopron megye</option>
    <option >Hajdú-Bihar megye</option>
    <option >Heves megye</option>
    <option >Jász-Nagykun-Szolnok megye</option>
    <option>Komárom-Esztergom megye</option>
    <option >Nógrád megye</option>
    <option >Pest megye</option>
    <option >Somogy megye</option>
    <option >Szabolcs-Szatmár-Bereg megye</option>
    <option >Tolna megye</option>
    <option >Vas megye</option>
    <option >Veszprém megye</option>
    <option >Zala megye</option>                                               
</select>
{!! $errors->first('county', '<p class="help-block">:message</p>') !!}


<br><br>Kép *:
<input type="file" name="image">
{!! $errors->first('image', '<p class="help-block">:message</p>') !!}

<br><br>Kiemelt: 
<select name="emphasized">
        <option value="false">Nem</option>
        <option value="true">Igen</option>
</select>
<br><br>Galéria:
<input type="file" name="gallery[]">
<input type="file" name="gallery[]">
<input type="file" name="gallery[]">
<input type="file" name="gallery[]">
{!! $errors->first('gallery.*', '<p class="help-block">:message</p>') !!}


<br><br>Leírás:
<textarea class="textarea" name="description">{!! old('description') !!}</textarea>

<br><br>Étel 1:
Név:
<input type="text" name="food_one" value="{{ old('food_one') }}">
Részletek:
<textarea rows="3" cols="20" name="food_one_desc">{!! old('food_one_desc') !!}</textarea>

Ár:
<input type="number" name="food_one_price" value="{{ old('food_one_price') }}"> Ft
{!! $errors->first('food_one_price', '<p class="help-block">:message</p>') !!}


<br><br>Étel 2:
Név:
<input type="text" name="food_two" value="{{ old('food_two') }}">
Részletek:
<textarea rows="3" cols="20" name="food_two_desc">{!! old('food_two_desc') !!}</textarea>

Ár:
<input type="number" name="food_two_price" value="{{ old('food_two_price') }}"> Ft
{!! $errors->first('food_two_price', '<p class="help-block">:message</p>') !!}

<br><br>Étel 3:
Név:
<input type="text" name="food_three" value="{{ old('food_three') }}">
Részletek:
<textarea rows="3" cols="20" name="food_three_desc">{!! old('food_three_desc') !!}</textarea>

Ár:
<input type="number" name="food_three_price" value="{{ old('food_three_price') }}"> Ft
{!! $errors->first('food_three_price', '<p class="help-block">:message</p>') !!}


<br><br>Étel 4:
Név:
<input type="text" name="food_four" value="{{ old('food_four') }}">
Részletek:
<textarea rows="3" cols="20" name="food_four_desc">{!! old('food_four_desc') !!}</textarea>

Ár:
<input type="number" name="food_four_price" value="{{ old('food_four_price') }}"> Ft
{!! $errors->first('food_four_price', '<p class="help-block">:message</p>') !!}


<br><br><h3>Értékelések:</h3>

<br><br><h4>Étel:</h4>
Szöveges értékelés:
<textarea class="textarea" name="food_rate_desc">{!! old('food_rate_desc') !!}</textarea><br>
Pontszám*:
<input type="number" name="food_rate" value="{{ old('food_rate') }}">
{!! $errors->first('food_rate', '<p class="help-block">:message</p>') !!}


<br><br><h4>Felszolgálás:</h4>
Szöveges értékelés:
<textarea class="textarea" name="service_rate_desc">{!! old('service_rate_desc') !!}</textarea><br>
Pontszám*:
<input type="number" name="service_rate" value="{{ old('service_rate') }}">
{!! $errors->first('service_rate', '<p class="help-block">:message</p>') !!}


<br><br><h4>Étterem:</h4>
Szöveges értékelés:
<textarea class="textarea" name="place_rate_desc">{!! old('place_rate_desc') !!}</textarea><br>
Pontszám*:
<input type="number" name="place_rate" value="{{ old('place_rate') }}">
{!! $errors->first('place_rate', '<p class="help-block">:message</p>') !!}


<br><br>Cím:
<input type="text" name="address" value="{{ old('address') }}">

<br><br>URL:
<input type="text" name="url" value="{{ old('url') }}">

<br><br><h3>Jellemzők:</h3><br>
@foreach($features as $feature)
    <label><input type="checkbox" name="features[]" value="{{$feature->id}}"> {{$feature->name}}</label>
    <br>
  @endforeach

<br><input class="btn" type="submit" value="Hozzáadás">

</form>
</div>


@endsection

