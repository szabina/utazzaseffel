<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>404 error</title>

    <script src="https://kit.fontawesome.com/9760bb6d35.js"></script>
    <link rel="stylesheet" href="{{asset('realfontawesome.css') }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/icons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/icons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/icons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/icons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/icons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('img/icons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('img/icons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fontawsome -->


    <link href="{{asset('css/random/allMin.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('custom.css')}}">
   

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="dorne-load"></div>
    </div>

    <!-- ***** Search Form Area ***** -->
    <div class="dorne-search-form d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-close-btn" id="closeBtn">
                        <i class="pe-7s-close-circle" aria-hidden="true"></i>
                    </div>
                    <form action="{{route('restaurants.search')}}" method="get">
                        <input type="search" name="top_search" id="search"
                            placeholder="Keresés éttermek, helyszínek, ételek, leírások közt...">
                        <input type="submit" class="d-none" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area" id="header">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg">
                        <a class="navbar-brand" href="{{route('homepage')}}">
                            Utazz a Séffel
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dorneNav"
                            aria-controls="dorneNav" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="fa fa-bars"></span></button>
                        <!-- Nav -->
                        <div class="collapse navbar-collapse" id="dorneNav">
                            <ul class="navbar-nav mr-auto" id="dorneMenu">
                                    <li class="nav-item">
                                            <a class="nav-link" href="{{route('homepage')}}">Főoldal <span
                                                    class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('faq')}}">Gy.i.k</a>
                                        </li>
                                        <li class="nav-item d-lg-none">
                                            <a class="nav-link" href="{{route('restaurants.byCategories', ['category_id' => 1])}}">Nemzetközi / Street food</a>
                                        </li>
                                        <li class="nav-item d-lg-none">
                                            <a class="nav-link" href="{{route('restaurants.byCategories', ['category_id' => 2])}}">Modern, fine dining</a>
                                        </li>
                                        <li class="nav-item d-lg-none">
                                            <a class="nav-link" href="{{route('restaurants.byCategories', ['category_id' => 3])}}">Magyaros éttermek</a>
                                        </li>
                                        <li class="nav-item d-lg-none">
                                            <a class="nav-link" href="{{route('restaurants.byCategories', ['category_id' => 4])}}">Cukrászda</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('contact')}}">Kapcsolat</a>
                                        </li>
                            </ul>
                            <!-- Search btn -->
                            <div class="dorne-search-btn">
                                <a id="search-btn" href="#"><i class="fa fa-search" aria-hidden="true"></i> Keresés</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('img/hero2.jpg')}})">
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="error-container">
        <p>404-es hiba!</p>
        <p>Ez az oldal sajnos nem létezik</p>
        <p>Válassz egy másik tartalmat a felső menüsor segítségével!</p>
    </div>
    

        <!-- ****** Footer Area Start ****** -->
        @include('frontend.footer')
</body>

</html>