<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;


class EditRestaurantValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'city' => 'required',
            'county' => 'required',
            'category_id' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            'food_rate' => 'required|integer|gte:0|max:10',
            'service_rate' => 'required|integer|gte:0|max:10',
            'place_rate' => 'required|integer|gte:0|max:10',
           
        ]);

        if ($validator->fails()) {
            \Session::flash('error-msg', 'Módosítás nem sikerült!');
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        };
        return $next($request);
    }
}
