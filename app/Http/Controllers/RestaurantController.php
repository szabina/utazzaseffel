<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;


use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Feature;
use App\Models\Restaurant;
use App\Models\Ad;


class RestaurantController extends Controller
{

    //Admin oldalon az éttermek listája keresés alapján is
    public function list(Request $req){
        $categories = Category::all();
        $restaurants = Restaurant::search($req->all())->get();
        return view('admin.restaurants.list')->with('restaurants', $restaurants)->with('categories', $categories);
    }

    public function create(){
        $categories = Category::all();
        $features = Feature::all();
        return view('admin.restaurants.create')->with('categories', $categories)->with('features', $features);
    }
    public function store(Request $req){
        $restaurant = new Restaurant;
        $restaurant->setAttributes($req, $restaurant);
        $restaurant->save();
        $restaurant->features()->attach($req->input('features'));
        \Session::flash('message', 'Sikeres mentés!'); 
        return redirect()->route('restaurants.list');
    }
    public function edit(string $id){
        $restaurant = Restaurant::find($id);
        $categories = Category::all();
        $features = Feature::all();
        return view('admin.restaurants.edit')->with('restaurant', $restaurant)->with('categories', $categories)->with('features', $features);
    }
    public function update($id, Request $req){

        $restaurant = Restaurant::find($id);
        $restaurant->setAttributes($req, $restaurant);
        $restaurant->save();
        $restaurant->features()->sync($req->input('features'));
        \Session::flash('message', 'Sikeres módosítás!'); 
        return redirect()->route('restaurants.list');

    }
    public function delete (string $id){
        $restaurant = Restaurant::find($id);
        $restaurant->features()->detach($restaurant->features);
        $restaurant->delete();
        if($restaurant->delete()){
        \Session::flash('message', 'Sikeres törlés!'); 
        return redirect()->back();
        } else {
            return redirect()->route('restaurants.list');
        }
    }

    //Kiemelt éttermek választása
    public function selectEmphasized(){
        $restaurants = Restaurant::orderBy('name', 'asc')->get();
        return view('admin.restaurants.emphasized')->with('restaurants', $restaurants);

    }
    //A választott éttermek mentése kiemeltként, a többiek mentése nem kiemeltként
    public function saveEmphasized(Request $req){
        $validator = Validator::make($req->all(), [
            'emphasized' => 'required|array',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        };
        $chosenRestaurants = $req->input('emphasized');
        $allRestaurants = Restaurant::all();
        foreach ($allRestaurants as $restaurant) {
            if(in_array($restaurant->id, $chosenRestaurants)){
                $restaurant->emphasized = true;
                $restaurant->save();
            } else {
                $restaurant->emphasized = false;
                $restaurant->save();
            }
        }
        \Session::flash('message', 'Sikeres mentés!'); 
        return redirect()->back();
    }

    //Nyilvános: az étterem részletei
    public function show(string $category_slug, string $restaurant_slug){
        $categories = Category::all();
        $restaurant = Restaurant::where('slug', $restaurant_slug)->first();
        $features = Feature::all();
        $ads = Ad::all();
        return view('frontend.single')->with('restaurant', $restaurant)->with('categories', $categories)->with('features', $features)->with('ads', $ads);
    }
    //Nyilvános: az éttermek listája a 4 alap kategória alapján
    public function categorySearch($slug){
        $category = Category::where('slug_name', $slug)->first();
        $category_id = $category->id;
        $restaurants = Restaurant::where('category_id', $category_id)->orderBy('name', 'asc')->get();
        $categories = Category::all();
        $features = Feature::all();
        $ads = Ad::all();
        return view('frontend.list')->with('restaurants', $restaurants)->with('category', $category)->with('categories', $categories)->with('features', $features)->with('ads', $ads);
    }
    //Nyilvános: az éttermek listája a keresők beállításai alapján (főoldalon; navbarban lévő kereső; aloldalakon az oldalt lévő kereső)
    public function search(Request $req){
        
        $categories = Category::all();
        $features = Feature::all();
        $ads = Ad::all();
        $restaurants = Restaurant::search($req->all())->orderBy('name', 'asc')->get();
        return view('frontend.results')->with('restaurants', $restaurants)->with('categories', $categories)->with('features', $features)->with('ads', $ads);
    }
}
