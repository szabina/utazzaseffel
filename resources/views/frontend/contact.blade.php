@extends('frontend.inc')

    @section('content')

     <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(img/hero2.jpg)">
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ***** Contact Area Start ***** -->
    <div class="dorne-contact-area d-md-flex" id="contact">
                            
        <!-- Contact Form Area -->
        <div class="contact-form-area equal-height">
            <div class="contact-text">
                <h4>Hello, lépj kapcsolatba velünk</h4>
                <div class="contact-info d-lg-flex">
                    <div class="single-contact-info">
                        <h6><i class="fa fa-envelope-o" aria-hidden="true"></i>utazzaseffel@gmail.com</h6>
                        <h6><i class="fa fa-phone" aria-hidden="true"></i> 0670/244-6796</h6>
                    </div>
                </div>
            </div>
            <div class="contact-form">
                <div class="contact-form-title">
                    <h6>Küldj üzenetet</h6>
                </div>
                <form action="{{route('message.send')}}" method="POST">
                @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 px-0" >
                            <div class="col-12">
                                <p class="error">{!! $errors->first('name', '<p class="help-block">:message</p>') !!}</p>
                                <input type="text" name="name" class="form-control has-error" placeholder="Név">
                                
                            </div>
                            <div class="col-12">
                                <p class="error">{!! $errors->first('email', '<p class="help-block">:message</p>') !!}</p>
                                <input type="email" name="email" class="form-control has-error" placeholder="E-mail cím">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <p class="error">{!! $errors->first('message', '<p class="help-block">:message</p>') !!}</p>
                            <textarea name="message" class="form-control has-error" id="Message" cols="30" rows="12"
                                placeholder="Üzenet"></textarea>
                        </div>
                        <div class="form-check col-12">
                            <p class="error">{!! $errors->first('checkbox', '<p class="help-block">:message</p>') !!}</p>
                            <div class="form-check-row" >
                                <input type="checkbox" class="form-check-input has-error" name="checkbox" id="contactForm-check">
                                <label class="form-check-label" for="contactForm-check"></label> 
                                <p class="form-check-p">
                                    Elfogadtam az <a href="#"> adatvédelmi nyilatkozatot</a>.
                                </p> 
                            </div>
                        </div>
                        <div class="col-12">
                            
                            <button type="submit" class="btn dorne-btn">Küldés</button>
                            @if (session('status'))
                            <br><br>
                                <p class="">
                                    {{ session('status') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ***** Contact Area End ***** -->


@endsection



    