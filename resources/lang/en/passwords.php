<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszó min. 8 karekter legyen, és egyezniük kell!',
    'reset' => 'A jelszó mentése sikerült!',
    'sent' => 'Az email elküldve!',
    'token' => 'This password reset token is invalid.',
    'user' => "Nem regisztrált email cím!",

];
