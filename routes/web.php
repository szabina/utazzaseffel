<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Symlink create
Route::get('/any-route', function () {
    Artisan::call('storage:link');
});



Route::get('/admin', 'Admin\AdminController@loginForm')->name('admin.loginForm');
Route::post('/admin', 'Admin\AdminController@login')->name('admin.login')->middleware('inputCheckforScriptTag')->middleware('loginFormValidator');

Route::post('/password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email')->middleware('inputCheckforScriptTag');
Route::get('/password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset','Auth\ResetPasswordController@reset')->middleware('inputCheckforScriptTag');
Route::get('/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
 
//Admin belépés után látogatható oldalak
Route::group(['middleware' => 'adminAuth'], function(){
    Route::delete('/admin/logout', 'Admin\AdminController@logout')->name('admin.logout');
    Route::get('/dashboard/{admin_id}', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('/profile/{admin_id}', 'Admin\AdminController@show')->name('admin.profile');
    Route::get('/new/{admin_id}', 'Admin\AdminController@create')->name('admin.create');
    Route::post('/new', 'Admin\AdminController@store')->name('admin.store')->middleware('inputCheckforScriptTag');
    Route::post('/update/{admin_id}', 'Admin\AdminController@update')->name('admin.update')->middleware('inputCheckforScriptTag');
    Route::get('/admin_list/{admin_id}', 'Admin\AdminController@list')->name('admin.list');
    Route::get('/edit/{admin_id}', 'Admin\AdminController@edit')->name('admin.edit');
    Route::delete('/delete_admin/{ad_id}', 'Admin\AdminController@delete')->name('admin.delete');
    
    Route::group(['prefix' => 'ad'], function (){
        Route::get('/list', 'AdController@list')->name('ads.list');
        Route::get('/new', 'AdController@create')->name('ad.create');
        Route::post('/new', 'AdController@store')->name('ad.store')->middleware('inputCheckforScriptTag')->middleware('adValidator');
        Route::get('/edit/{ad_id}', 'AdController@edit')->name('ad.edit');
        Route::post('/update/{ad_id}', 'AdController@update')->name('ad.update')->middleware('inputCheckforScriptTag');
        Route::delete('/delete/{ad_id}', 'AdController@delete')->name('ad.delete');
    
    });
    Route::group(['prefix' => 'category'], function (){
        Route::get('/list', 'CategoryController@list')->name('categories.list');
        Route::get('/new', 'CategoryController@create')->name('category.create');
        Route::post('/new', 'CategoryController@store')->name('category.store')->middleware('inputCheckforScriptTag');
        Route::delete('/delete/{cat_id}', 'CategoryController@delete')->name('category.delete');
    });
    /* Route::group(['prefix' => 'feature'], function (){
        Route::get('/list', 'FeatureController@list')->name('features.list');
        Route::get('/new', 'FeatureController@create')->name('feature.create');
        Route::post('/new', 'FeatureController@store')->name('feature.store')->middleware('inputCheckforScriptTag');
        Route::delete('/delete/{feature_id}', 'FeatureController@delete')->name('feature.delete');
    }); */
    Route::group(['prefix' => 'restaurant'], function (){
        Route::get('/list', 'RestaurantController@list')->name('restaurants.list');
        Route::get('/new', 'RestaurantController@create')->name('restaurant.create');
        Route::post('/new', 'RestaurantController@store')->name('restaurant.store')->middleware('inputCheckforScriptTag')->middleware('restaurantInputValidator');
        Route::get('/edit/{restaurant_id}', 'RestaurantController@edit')->name('restaurant.edit');
        Route::post('/update/{restaurant_id}', 'RestaurantController@update')->name('restaurant.update')->middleware('inputCheckforScriptTag')->middleware('editRestaurantValidator');
        Route::delete('/delete/{restaurant_id}', 'RestaurantController@delete')->name('restaurant.delete');
        Route::get('/emphasized', 'RestaurantController@selectEmphasized')->name('restaurant.selectEmphasized');
        Route::post('/emphasized', 'RestaurantController@saveEmphasized')->name('restaurant.saveEmphasized');

    });
    
    Route::group(['prefix' => 'message'], function(){
        Route::get('/list', 'MessageController@list')->name('messages.list');
        Route::get('/details/{message_id}', 'MessageController@show')->name('message.show');
        Route::post('/resolve/{message_id}', 'MessageController@resolve')->name('message.resolve');
        Route::delete('/details/{message_id}', 'MessageController@delete')->name('message.delete');
    
    });
});

//Nyilvános oldalak
Route::get('/', 'PagesController@index')->name('homepage');
Route::get('/gyik', 'PagesController@faq')->name('faq');
Route::get('/search', 'RestaurantController@search')->name('restaurants.search');
Route::get('/kapcsolat', 'ContactController@contactForm')->name('contact');
Route::post('/contact', 'ContactController@send')->name('message.send')->middleware('inputCheckforScriptTag')->middleware('formValidator');
Route::get('/{category_slug}/{restaurant_slug}', 'RestaurantController@show')->name('restaurant.show');
Route::get('/{category_slug}', 'RestaurantController@categorySearch')->name('restaurants.byCategories');
