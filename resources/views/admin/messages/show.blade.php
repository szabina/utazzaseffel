@extends('admin.layouts.template')

@section('content')

<h1>Üzenet részletei:</h1>

 <ul>
        <li>Érkezett: {{$message->created_at}}<br>
        Email: {{$message->email}}<br>
        Név: {{$message->name}}<br>
        Elintézve: 
        @if($message->resolved)
        {{'Igen'}}
        @else
        {{'Nem'}}
        @endif
        </li>
        Üzenet:
        <p>{{$message->message}}</p>
        @if($message->resolved != true)
        <form action="{{route('message.resolve', ['message_id' => $message->id])}}" method="POST">
                @csrf  
            <input type="hidden" value="{{$message->id}}" name="message_id">
            <input class="btn btn-success" type="submit" value="Elintézettnek jelölöm">
        </form> <br>
        @endif       
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        <form class="delete" action="{{route('message.delete', ['message_id' => $message->id])}}" method="POST">
                @csrf  
            <input type="hidden" value="DELETE" name="_method">
            <input class="btn btn-danger" type="submit" value="Törlés">
        </form>
    </ul>

@endsection