@extends('admin.layouts.template')

@section('content')

<h1>Hirdetés módosítása</h1>
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
@if(Session::has('error-msg'))
<p class="alert alert-danger">{{ Session::get('error-msg') }}</p>
@endif

<form action="{{route('ad.update', ['ad_id' => $ad->id])}}" method="POST" enctype="multipart/form-data">
@csrf

URL *:
<input type="text" name="url" value="{{ old('url') ?: $ad->url}}">
{!! $errors->first('url', '<p class="help-block">:message</p>') !!}
<br><br>

Cím *:
<input type="text" name="title" value="{{ old('title') ?: $ad->title}}">
{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
<br><br>

Alcím:
<input type="text" name="sub_title" value="{{ old('sub_title') ?: $ad->sub_title}}"><br><br>
Új kép:
<input type="file" name="image">
{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
<br><br>

<input type="submit" value="Módosítás">

</form>
<br>
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif

@endsection