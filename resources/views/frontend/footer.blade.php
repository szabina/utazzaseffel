<footer class="dorne-footer-area">
        <div class="container-fluid">
            <div class="row footer-row">
                <div class="col-12 col-md-4">
                    <div class="footer-wrapper footer-wrapper-left">
                        <h4 class="footer-title">
                            Éttermek
                        </h4>
                        <ul class="footer-list">
                            <li class="footer-list-item">
                                <a href="{{route('restaurants.byCategories', ['category_slug' => 'nemzetkozi-es-streetfood'])}}" class="footer-link">Nemzetközi / Street food</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="{{route('restaurants.byCategories', ['category_slug' => 'modern-fine-dining-ettermek'])}}" class="footer-link">Modern, fine dining</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="{{route('restaurants.byCategories', ['category_slug' => 'magyaros-ettermek'])}}" class="footer-link">Magyaros éttermek</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="{{route('restaurants.byCategories', ['category_slug' => 'cukraszda'])}}" class="footer-link">Cukrászda</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-none d-md-block col-md-4">
                    <div class="footer-img-wrapper">
                        <img src="{{asset('img/utazz-a-seffel-logo-png-03.png')}}" alt="utazz-a-seffel-logo-png-03"
                            class="footer-img">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-wrapper">
                        <h4 class="footer-title">
                            Elérhetőségek
                        </h4>
                        <ul class="footer-list footer-list-social">
                            <li class="footer-list-item">
                                <a href="tel:06702446796" class="footer-link"><i class="fa fa-phone"></i>06702446796</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="mailto:utazzaseffel@gmail.com" class="footer-link"><i
                                        class="fa fa-envelope-o"></i>utazzaseffel@gmail.com</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="https://www.facebook.com/%C3%89tteremkalauz-utazz-a-s%C3%A9ffel-312768186300922/"
                                    class="footer-link"><i class="fab fa-facebook-f"></i>Étteremkalauz - Utazz a
                                    séffel</a>
                            </li>
                            <li class="footer-list-item">
                                <a href="https://www.facebook.com/%C3%89tteremkalauz-utazz-a-s%C3%A9ffel-312768186300922/"
                                    class="footer-link"><i class="fab fa-instagram"></i>Utazz a Séffel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-md-flex align-items-center justify-content-center">
                    <div class="footer-text">
                        <p>
                            Copyright&#169; Utazz a Séffel | A weboldal karbantartója a
                            <a href="https://proonline.hu/weboldal-arak " class="footer-link footer-link-small"
                                target="_blank">Pro Online Webfejlesztő</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ****** Footer Area End ****** -->

    <!-- jQuery-2.2.4 js -->
    <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('js/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap-4 js -->
    <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{asset('js/others/plugins.js')}}"></script>
    <!-- Google Maps js -->
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk9KNSL1jTv4MY9Pza6w8DJkpI_nHyCnk"></script> --}}
    {{-- <script src="js/google-map/explore-map-active.js"></script> --}}
    <!-- Active JS -->
    <script src="{{asset('js/active.js')}}"></script>

    <!-- <a id="scrollUp" href="#top" style="position: fixed; z-index: 998; display: none;">
        <i class="pe-7s-angle-up" aria-hidden="true"></i>
    </a> -->


    <!-- <div id="bh-simple-helper" class="bh-tr">
        <div class="visible-xs  d-block d-sm-none">XS</div>
        <div class="visible-sm  d-none d-sm-block d-md-none">SM</div>
        <div class="visible-md  d-none d-md-block d-lg-none">MD</div>
        <div class="visible-lg  d-none d-lg-block d-xl-none">LG</div>
        <div class="visible-lg  d-none d-xl-block">XL</div>
    </div> -->