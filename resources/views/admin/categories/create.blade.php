@extends('admin.layouts.template')

@section('content')
  
    
<h1>Kategória hozzáadása</h1><br>
@if(Session::has('error-msg'))
<p class="alert alert-danger">{{ Session::get('error-msg') }}</p>
@endif

<form action="{{route('category.store')}}" method="POST">
@csrf
Név:
<input type="text" name="name">
{!! $errors->first('name', '<p class="help-block">:message</p>') !!}

<input type="submit" value="Hozzáadás">

</form><br>
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif

@endsection