@extends('admin.layouts.template')

@section('content')
<h1>Adminok</h1>

@if(Session::has('message'))
<p class="alert alert-danger">{{ Session::get('message') }}</p>
@endif


@foreach ($admins as $admin)
<div style="display:flex; flex-direction:row; justify-content: space-between; border-bottom: 1px solid grey; height: 150px; padding:10px">

    <div>
      Név: {{$admin->name}}<br>
      Email: {{$admin->email}}<br>
      Szerepkör: {{$admin->role['name']}}<br>
      @if(auth()->guard('admin')->user()->hasPermission(\App\Users\Models\Permission::SEE_USERS))

      Jogosultságok: 
      @foreach ($admin->role->permissionsList() as $permission)
              {{$permission}}; 
        @endforeach
    @endif
</div>


<div>
@if(auth()->guard('admin')->user()->hasPermission(\App\Users\Models\Permission::EDIT_USERS))
        <a style="margin-bottom:5px" class="btn btn-success" href="{{route('admin.edit', ['admin_id' => $admin->id])}}">Módosítás</a>
        <form class="delete" action="{{route('admin.delete', ['admin_id' => $admin->id])}}" method="POST">
            @csrf
        
          <input type="hidden" name="_method" value="DELETE">        
        <button type="submit" class="btn btn-danger">Törlés</button>
      </form>
@endif</div>

</div><br>
    
@endforeach

@endsection
