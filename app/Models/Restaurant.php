<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $casts = [
        'gallery' => 'array'
    ];

    //Az oldalon a navbarban lévő kereső ezekben az oszlopokban fog keresni
    public $searchables = ['name', 'county', 'city', 'description', 'food_one', 'food_one_desc','food_two', 'food_two_desc', 'food_three', 'food_three_desc',
    'food_four', 'food_four_desc', 'food_rate_desc', 'service_rate_desc','place_rate_desc', 'address'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function features(){
        return $this->belongsToMany(Feature::class);
    }
    //Kép feltöltés
    public function saveImage($req){
        //Handle file upload
        if($req->hasFile('image')){
            //Get filename with extension
            $fullFilename = $req->file('image')->getClientOriginalName();
            //Get just file name
            $filename = pathinfo($fullFilename, PATHINFO_FILENAME );
            //Get just extension
            $ext = $req->file('image')->getClientOriginalExtension();

            $filenameToStore = $filename.'_'.time().'.'.$ext;

            $path = $req->file('image')->storeAs('public/restaurants', $filenameToStore);
            return $filenameToStore;

        };
    }
    public function setAttributes($req, $restaurant){
        $restaurant->name = $req->input('name');
        $restaurant->city = $req->input('city');
        $restaurant->category()->associate($req->input('category_id'));
        $restaurant->county = $req->input('county');

        if($req->hasFile('image')){
            $restaurant->image = $this->saveImage($req);
        };
         $restaurant->emphasized = (bool)$req->input('emphasized');
        //Store method ban a galéria felvétele
        if($req->hasFile('gallery')){
            $gallery = [];
         foreach ($req->file('gallery') as $file) {

            $fullFilename = $file->getClientOriginalName();
            $filename = pathinfo($fullFilename, PATHINFO_FILENAME );
            $ext = $file->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$ext;

            $path = $file->storeAs('public/restaurants', $filenameToStore);
            $gallery[] = $filenameToStore;
        };

        $restaurant->gallery = $gallery;
        };

        //Update nél lehetőség van a megtartott régi képek és az újonnan feltöltött galéria együttes elmentésére
        if($req->input('oldGallery') || $req->hasFile('newGallery'))
        {
            $syncedGallery = [];

            if($req->input('oldGallery')){
                foreach ($req->input('oldGallery') as $image) {
                $syncedGallery[] = $image;
                }
            }

            if($req->hasFile('newGallery')){

                foreach ($req->file('newGallery') as $file) {

                $fullFilename = $file->getClientOriginalName();
                $filename = pathinfo($fullFilename, PATHINFO_FILENAME );
                $ext = $file->getClientOriginalExtension();
                $filenameToStore = $filename.'_'.time().'.'.$ext;

                $path = $file->storeAs('public/restaurants', $filenameToStore);
                $syncedGallery[] = $filenameToStore;
                };

            };
            $restaurant->gallery = $syncedGallery;

        }

        $restaurant->description = $req->input('description');
        $restaurant->food_one = $req->input('food_one');
        $restaurant->food_one_desc = $req->input('food_one_desc');
        $restaurant->food_one_price = $req->input('food_one_price');

        $restaurant->food_two = $req->input('food_two');
        $restaurant->food_two_desc = $req->input('food_two_desc');
        $restaurant->food_two_price = $req->input('food_two_price');

        $restaurant->food_three = $req->input('food_three');
        $restaurant->food_three_desc = $req->input('food_three_desc');
        $restaurant->food_three_price = $req->input('food_three_price');

        $restaurant->food_four = $req->input('food_four');
        $restaurant->food_four_desc = $req->input('food_four_desc');
        $restaurant->food_four_price = $req->input('food_four_price');

        $restaurant->food_rate_desc = $req->input('food_rate_desc');
        $restaurant->food_rate = $req->input('food_rate');

        $restaurant->service_rate_desc = $req->input('service_rate_desc');
        $restaurant->service_rate = $req->input('service_rate');

        $restaurant->place_rate_desc = $req->input('place_rate_desc');
        $restaurant->place_rate = $req->input('place_rate');

        $restaurant->address = $req->input('address');
        $restaurant->url = $this->urlCorrection($req->input('url') ?: '');

        $restaurant->slug = \Str::slug($req->input('name'));
        $restaurant->overall_rate = $this->calculateOverallRate($restaurant->food_rate, $restaurant->service_rate, $restaurant->place_rate);


        return $restaurant;
    }

    public function calculateOverallRate($food_rate, $service_rate, $place_rate){

        $result = ($food_rate * 8 + $service_rate + $place_rate) / 10;

        return (int)round($result, 0);

    }
    public function urlCorrection(string $inputUrl){
        if (strpos($inputUrl, 'http') !== false) {
            return $inputUrl;
        }
        else {
            return '//'.$inputUrl;
        }
    }

    public function hasFeature($featureId){
        return in_array($featureId, $this->features()->pluck('id')->toArray());
    }

    public function getCategorySlug(){
        $category = $this->category;
        return $category->slug_name;

    }

    public function scopeSearch($query, $request){
        //Kereső gomb felül ill. admin oldalon az éttermek kezelése menüpontban, a $searchables oszlopokban keres
        if(isset($request['top_search'])){
            $searchQuery = $request['top_search'];
            $requestData = $this->searchables;
            $query->where(function($q) use($requestData, $searchQuery) {
                        foreach ($requestData as $field)
                           $q->orWhere($field, 'like', "%{$searchQuery}%");
                        })->get();
        }
        //Főoldalon és az aloldalakon az oldalt lévő kereső, valamint az admin oldalon lévő kereső
        if(isset($request['search'])){
            $params = $request['search'];


        if(isset($params['county']) &&  $params['county'] != 'Kiválasztás' ) {
            $query->where('county', 'LIKE', '%'.$params['county'].'%');

          }
          if(isset($params['category']) && $params['category'] != 'Kiválasztás') {

            $query->whereHas('category', function($q) use ($params) {
              $q->where('category_id', 'LIKE', '%'.$params['category'].'%');
            });

          }
          if(isset($params['rate']) &&  $params['rate'] != 'Értékelés' ) {
            $query->where('overall_rate', 'LIKE', '%'.$params['rate'].'%');
          }
          //Admin listában a rendezés
          if(isset($params['order_by'])) {
            $direction = isset($params['sort_dir']) ? $params['sort_dir'] : 'asc';
            $query->orderBy($params['order_by'], $direction);
          }


        }
          //Aloldalon oldalt lévő kereső, amiben egyszerre több kategóriát lehet választani
          if(isset($request['multiple_category'])){
            foreach ($request['multiple_category'] as $index => $inputId) {
                if($index == 0){
                  $query->whereHas('category', function($q) use ($inputId) {
                      $q->where('category_id', 'LIKE', '%'.$inputId.'%');
                    });

                } else {
                  $query->orWhereHas('category', function($q) use ($inputId) {
                      $q->where('category_id', 'LIKE', '%'.$inputId.'%');
                  });
              }

            }

        }




    }
}
