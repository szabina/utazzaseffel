<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;


class MessageController extends Controller
{
    public function list(Request $req){
        //Elintézendő üzik mutatása:
        if($req->input('unresolved')){
        
            $messages = Message::where('resolved', false)->orderBy('created_at', 'desc')->get();
        } 
        
        else {
            $messages = Message::orderBy('created_at', 'desc')->get();
        };
        return view('admin.messages.list')->with('messages', $messages);
    }

    public function show(string $id){
        $message = Message::find($id);
        return view('admin.messages.show')->with('message', $message);
    }

    //Üzik átállítása elintézettre
    public function resolve(Request $req){
        $message = Message::find($req->input('message_id'));
        $message->resolved = true;
        $message->save();
        \Session::flash('message', 'Sikeres módosítás!'); 
        return redirect()->route('messages.list');
        }
    public function delete(string $id){
        $message = Message::find($id);
        $message->delete();
        return redirect()->route('messages.list');
    }
}
