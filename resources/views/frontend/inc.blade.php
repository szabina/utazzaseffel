<!DOCTYPE html>
<html lang="hu">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="Jakab László Szocska Szabina">

    <meta name="google-site-verification" content="GuV_MmhYVmq_h4iUTMIlZnvuB2IZdNXEGU3j5Xr3wzI" />
    
    <!-- Title -->
    <title>Utazz a séffel | 
    @if(Route::current()->getName() == 'homepage')
            {{'Főoldal'}}
    
    @elseif(Route::current()->getName() == 'contact')
            {{'Kapcsolat'}}
    
    @elseif(Route::current()->parameters() == ['category_slug' => 'nemzetkozi-es-streetfood'])
            {{'Nemzetközi / street food'}}
    @elseif(Route::current()->parameters() == ['category_slug' => 'modern-fine-dining-ettermek'])
            {{'Modern, fine dining'}}
    @elseif(Route::current()->parameters() == ['category_slug' => 'magyaros-ettermek'])
            {{'Magyaros éttermek'}}
    
    @elseif(Route::current()->parameters() == ['category_slug' => 'cukraszda'])
            {{'Cukrászda'}}
    @else
            {{'Keresés eredménye'}}
    @endif
    </title>
   
    <link rel="stylesheet" href="{{asset('realfontawesome.css') }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/icons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/icons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/icons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/icons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/icons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('img/icons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('img/icons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fontawsome -->
        <link href="{{asset('fontawesome/css/all.css')}}" rel="stylesheet">



    <link href="{{asset('allMin.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('custom.css')}}">



</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="dorne-load"></div>
    </div>

   @include('frontend.navbar')
    <!-- ***** Header Area End ***** -->

    @yield('content')
    
    <!-- ****** Footer Area Start ****** -->
    @include('frontend.footer')
</body>



</html>