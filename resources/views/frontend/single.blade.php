<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="UTF-8">

  <meta name="description" content="">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="author" content="Jakab László Szocska Szabina">
  <!-- Title -->

  <title>Utazz a séffel | Éttermek</title>

  <!-- Favicon -->

  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/icons/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/icons/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icons/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/icons/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/icons/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icons/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/icons/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icons/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('img/icons/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{asset('img/icons/ms-icon-144x144.png')}}">
  <meta name="theme-color" content="#ffffff">

  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>

  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://kit.fontawesome.com/9760bb6d35.js"></script>

  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>

  <link href="{{asset('css/random/allMin.css')}}" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>

  <!-- Add the slick-theme.css if you want default styling -->

  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>

  <link rel="stylesheet" href="{{asset('custom.css')}}">

</head>

<body>

<!-- Preloader -->

<div id="preloader">

  <div class="dorne-load"></div>

</div>

@include('frontend.navbar')

<!-- ***** Header Area End ***** -->

<!-- ***** Breadcumb Area Start ***** -->

<div class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('img/hero2.jpg')}})">

</div>

<!-- ***** Breadcumb Area End ***** -->

<div class="container-fluid">

  <!-- Explore Area -->

  <section class="dorne-explore-area row">

    <!-- Explore Search Area -->

    <!-- Explore Search Form -->

    

          @include('frontend.sideSearch')

          
        </div></div>
        </div>

    <div class="col order-1 order-md-2 px-0 px-sm-4 py-3 col-8">

      <!--NAGY FEHÉR OSZLOP ELEJE-->

      <sec class="dorne-single-listing-area section-padding-100">

        <!-- ***** Single Listing Area Start ***** -->

        <div class="container-fluid">

          <div class="row-12">

            <!-- Single Listing Content -->

            <div class="col-12 px-6">

              <div class="slider-container">

                <div class="single-listing-content">
                    
                    <div><img src="/storage/restaurants/{{$restaurant->image}}" alt="" style="width:100%;height:100%;object-fit: cover;object-position:center" class="img-fluid"></div>

                  @if($restaurant->gallery)
                    @foreach ($restaurant->gallery as $image)
                      <div><img src="/storage/restaurants/{{$image}}" alt="" class="img-fluid" style="width:100%;height:100%;object-fit: cover;object-position:center"></div>
                    @endforeach
                  @endif

                </div>

              </div>

              <div class="listing-title">

                <h4>{{$restaurant->name}} | <br>{{$restaurant->city}} </h4>

                <h6>{{$restaurant->category->name}}</h6>

              </div>

              <div class="single-listing-nav">

                <nav>

                  <ul id="listingNav">

                    <li class="active"><a href="#overview">Leírás</a></li>

                    <li><a href="#menu">Étlap</a></li>

                    <li><a href="#review">Értékelés</a></li>

                  </ul>

                </nav>

              </div>

              <div class="overview-content mt-50 container" id="overview">

                <div class="row">

                  <div class="col-12 col-lg-6 px-3">

                    {!!$restaurant->description!!}

                  </div>

                  <div class="col-12 col-lg-6 px-3 checkboxcol">

                    <div class="kartyaszoveg"><h6>Összélmény <span>{{$restaurant->overall_rate}}p</span></h6>

                      <p>Étel {{$restaurant->food_rate}}p | </p>

                      <p>Felszolgálás {{$restaurant->service_rate}}p | </p>

                      <p>Étterem {{$restaurant->place_rate}}p</p><br>
                      <p>Cím: {{$restaurant->address}}</p>
                      </div>

                    <div class="row checkboxrow">
                      @foreach ($features as $feature)
                        <div class="col-6">

                          <input type="checkbox" id="{{$feature->name}}"
                                 {{$restaurant->hasFeature($feature->id) ? 'checked' : ''}}
                                 disabled>

                          <label for="{{$feature->name}}">{{$feature->name}}</label>
                        </div>
                      @endforeach

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

          <div class="listing-menu-area mt-100" id="menu">

            <h4>Étlap</h4>

            <!-- Single Listing Menu -->
            @if($restaurant->food_one)
              <div class="single-listing-menu d-flex justify-content-between">

                <!-- Listing Menu Title -->

                <div class="listing-menu-title">

                  <h6>{{$restaurant->food_one}}</h6>

                  <p>{{$restaurant->food_one_desc}}</p>

                </div>

                <!-- Listing Menu Price -->

                <div class="listing-menu-price">

                  <h6>{{$restaurant->food_one_price}} Huf</h6>

                </div>

              </div>
            @endif
          <!-- Single Listing Menu -->
            @if($restaurant->food_two)
              <div class="single-listing-menu d-flex justify-content-between">

                <!-- Listing Menu Title -->

                <div class="listing-menu-title">

                  <h6>{{$restaurant->food_two}}</h6>

                  <p>{{$restaurant->food_two_desc}}</p>

                </div>

                <!-- Listing Menu Price -->

                <div class="listing-menu-price">

                  <h6>{{$restaurant->food_two_price}} Huf</h6>

                </div>

              </div>
            @endif
          <!-- Single Listing Menu -->
            @if($restaurant->food_three)
              <div class="single-listing-menu d-flex justify-content-between">

                <!-- Listing Menu Title -->

                <div class="listing-menu-title">

                  <h6>{{$restaurant->food_three}}</h6>

                  <p>{{$restaurant->food_three_desc}}</p>

                </div>

                <!-- Listing Menu Price -->

                <div class="listing-menu-price">

                  <h6>{{$restaurant->food_three_price}} Huf</h6>

                </div>

              </div>
            @endif
          <!-- Single Listing Menu -->
            @if($restaurant->food_four)
              <div class="single-listing-menu d-flex justify-content-between">

                <!-- Listing Menu Title -->

                <div class="listing-menu-title">

                  <h6>{{$restaurant->food_four}}</h6>

                  <p>{{$restaurant->food_four_desc}}</p>

                </div>

                <!-- Listing Menu Price -->

                <div class="listing-menu-price">

                  <h6>{{$restaurant->food_four_price}} Huf</h6>

                </div>

              </div>
            @endif

          </div>

          <div class="listing-reviews-area mt-100" id="review">

            <h4>Értékelések</h4>

            <div class="single-review-area">

              <div class="reviewer-meta d-flex align-items-center">

                <div class="reviewer-content">

                  <div class="review-title-ratings d-flex justify-content-between">

                    <h5>Étel</h5>

                    <div class="ratings">

                      <p class="number-rate">10/{{$restaurant->food_rate}}</p>

                    </div>

                  </div>

                  <p>{!!$restaurant->food_rate_desc!!}</p>

                </div>

              </div>

            </div>

            <div class="single-review-area">

              <div class="reviewer-meta d-flex align-items-center">

                <div class="reviewer-content">

                  <div class="review-title-ratings d-flex justify-content-between">

                    <h5>Felszolgálás</h5>

                    <div class="ratings">

                      <p class="number-rate">10/{{$restaurant->service_rate}}</p>

                    </div>

                  </div>

                  <p>{!!$restaurant->service_rate_desc!!}</p>

                </div>

              </div>

            </div>

            <div class="single-review-area">

              <div class="reviewer-meta d-flex align-items-center">

                <div class="reviewer-content">

                  <div class="review-title-ratings d-flex justify-content-between">

                    <h5>Étterem</h5>

                    <div class="ratings">

                      <p class="number-rate">10/{{$restaurant->place_rate}}</p>

                    </div>

                  </div>

                  <p>{!!$restaurant->place_rate_desc!!}</p>

                </div>

              </div>

            </div>

            <span id="menualattigomb"><a target="_blank" href="{{$restaurant->url}}" class="btn dorne-btn mt-50">Tovább az

                                        étteremhez</a></span>

          </div>

        </div>

    </div>

</div>

<!-- ***** Single Listing Area End ***** -->

</section>

<!--NAGY FEHÉR OSZLOP VÉGE-->

</div>

</div>

</sec>

<!-- ****** Footer Area Start ****** -->

@include('frontend.footer')

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>

  $(document).ready(function () {

    $('.single-listing-content').slick({

      accessibility: true

    });

  });

</script>

</body>

</html>