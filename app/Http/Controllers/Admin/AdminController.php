<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users\Models\Admin;
use App\Users\Models\Role;

use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function loginForm(){
        return view('admin.admins.login');
    }
    public function login(Request $request){

        if (Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))) {
            $admin = Admin::where('email', 'LIKE', $request->input('email'))->first();

            return redirect()->route('admin.dashboard', array('admin_id' => $admin->id));
        }
        return redirect()->route('admin.loginForm');
    }
    public function logout(){
        auth()->guard('admin')->logout();
        return redirect()->route('admin.loginForm');
    }

    //admin dashboard
    public function index(string $id){
        if(auth()->guard('admin')->id() == $id){
        $admin = Admin::find($id);
        return view('admin.admins.dashboard')->with('admin', $admin);
        }
        return redirect()->route('admin.login');
    }

    //Admin hozzáadása ha rendelkezik: Permission '4' = EDIT_USERS
    public function create(string $id){
        if(auth()->guard('admin')->id() == $id){
            $loggedInAdmin = Admin::find($id);
            if($loggedInAdmin->hasPermission(4)){
                $admin = new Admin;
                $roles = Role::all();
                return view('admin.admins.create')->with('admin', $admin)->with('roles', $roles);
            }
        }
        return redirect()->route('admin.login');
    }
    public function store(Request $req){
        $this->validate($req, [
            'name' => 'required|min:4|unique:admins,name',
            'email' => 'required|email|unique:admins,email',
            'password' => 'confirmed|min:8',
            'role_id' => 'required|exists:roles,id'
          ]);

            $loggedInAdminId = auth()->guard('admin')->id();
            $loggedInAdmin = Admin::find($loggedInAdminId);
            if($loggedInAdmin->hasPermission(4)){
                $admin = new Admin;
                $admin->name = $req->input('name');
                $admin->email = $req->input('email');
                $admin->password = \Hash::make($req->input('password'));
                $admin->role()->associate($req->input('role_id'));
                $admin->save();
                \Session::flash('message', 'Sikeres mentés!'); 
                return redirect()->back();
            }
        
        return redirect()->route('admin.login');
        
    }
    public function list(){
        $admins = Admin::all();
        return view('admin.admins.list')->with('admins', $admins);
    }
    public function show(string $id){
        if(auth()->guard('admin')->id() == $id){
        $admin = Admin::find($id);
        $roles = Role::all();
        return view('admin.admins.profile')->with('admin', $admin)->with('roles', $roles);
        }
        return redirect()->back();
    }
        
    //Admin módosítása ha rendelkezik: Permission '4' = EDIT_USERS
    public function edit(string $id){
            $loggedInAdminId = auth()->guard('admin')->id();
            $loggedInAdmin = Admin::find($loggedInAdminId);
            
            if($loggedInAdmin->hasPermission(4)){
                $admin = Admin::find($id);
                $roles = Role::all();
                return view('admin.admins.edit')->with('admin', $admin)->with('roles', $roles);
            }
        
        return redirect()->route('admin.login');
        
    }

    //Admin módosítása ha rendelkezik: Permission '4' = EDIT_USERS, vagy a bejelentkezett felhasználó a saját adatait módosítja
    public function update(Request $req, $id){
        //belépett admin:
        $loggedInAdminId = auth()->guard('admin')->id();
        $loggedInAdmin = Admin::find($loggedInAdminId);
        //módosítandó admin:
        $admin = Admin::find($id);

        if($loggedInAdmin->hasPermission(4) || $admin->id == auth()->guard('admin')->id()){
        $this->validate($req, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'confirmed|min:8',
          ]);
    
        $admin->name = $req->input('name');
        $admin->email = $req->input('email');
        $admin->password = \Hash::make($req->input('password'));
        
        //1-es id-jú superadmin véletlenül se állítsa át magát adminra:
        if($admin->isDefaultSuperAdmin()){
        $admin->role()->associate(Role::SUPERADMIN);
        }
        else{
        $admin->role()->associate($req->input('role_id'));
        }

        $admin->save();
        \Session::flash('message', 'Sikeres módosítás!'); 
        return redirect()->back();
        }
        
        return redirect()->route('admin.login');
    }

    //Admin törlése ha rendelkezik: Permission '4' = EDIT_USERS, vagy a bejelentkezett felhasználó a saját magát törli
    public function delete (string $id){
        //belépett admin:
        $loggedInAdminId = auth()->guard('admin')->id();
        $loggedInAdmin = Admin::find($loggedInAdminId);
        //törlésre váró admin:
        $admin = Admin::find($id);
        
        if($loggedInAdmin->hasPermission(4) || $admin->id == auth()->guard('admin')->id()){
        
        //1-es id-jú admin ne törölje magát:
        if($admin->isDefaultSuperAdmin()){
            return redirect()->back();
        };

        $admin->delete();
        \Session::flash('message', 'Sikeres törlés!'); 
        return redirect()->back();
        }
        
        return redirect()->route('admin.login');
    }
}
