<?php

namespace App\Users\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    
    const SEE_PERMISSIONS = 1;
    const EDIT_PERMISSIONS = 2;
    const SEE_USERS = 3;
    const EDIT_USERS = 4;
    const SEE_ROLES = 5;
    const EDIT_ROLES = 6;
    const EDIT_CONTENT = 7;


    public function roles(){
        return $this->belongsToMany(Role::class)->withTimeStamps();
    }
}
