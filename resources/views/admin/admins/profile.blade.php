@extends('admin.layouts.template')

@section('content')
<h1>Profil</h1>

@include('admin.admins.form')
<br>
<form class="delete" action="{{route('admin.delete', ['admin_id' => $admin->id])}}" method="POST">
            @csrf
        
          <input type="hidden" name="_method" value="DELETE">        
        <button type="submit" class="btn btn-danger">Törlés</button>
      </form>

@endsection