<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

class CategoryController extends Controller
{
    
    public function list(){
        $categories = Category::all();
        return view('admin.categories.list')->with('categories', $categories);
    }
    public function create(){
        return view('admin.categories.create');
    }
    public function store(Request $req){
        $validator = Validator::make($req->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            \Session::flash('error-msg', 'Feltöltés nem sikerült!');
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }; 


        $category = new Category;
        $category->name = $req->input('name');
        $category->slug= 'v';
        $category->save();
        \Session::flash('message', 'Sikeres mentés!'); 
        return redirect()->route('categories.list');
    }
    public function delete(string $id){
        
        $category = Category::find($id);
        //Ha tartozik hozzá étterem, előbb legyen átállítva a típusuk egy másik kategóriára:
        $restaurants = $category->restaurants;
        if(count($restaurants) > 0){
            \Session::flash('alert', 'Kategória törlése előtt állítsa át a vonatkozó éttermek típusát, utána lehetővé válik a kategória törlése!'); 
            return redirect()->back(); 
        }
        //A 4 alap kategória ne legyen törölhető
        if($category->isDefault()){
            return redirect()->back();
        };
        $category->delete();
        \Session::flash('message', 'Sikeres törlés!'); 
        return redirect()->back();
    }
}
