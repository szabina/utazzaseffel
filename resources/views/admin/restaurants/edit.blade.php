@extends('admin.layouts.template')

@section('content')

<h1>Étterem módosítása</h1>
<div class="form-three widget-shadow">
<form class="form-horizontal" action="{{route('restaurant.update', ['restaurant_id' => $restaurant->id])}}" method="POST" enctype="multipart/form-data">
@csrf

@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
@if(Session::has('error-msg'))
<p class="alert alert-danger">{{ Session::get('error-msg') }}</p>
@endif

<br><br>Név *:
<input type="text" name="name" value="{{$restaurant->name}}">
{!! $errors->first('name', '<p class="help-block">:message</p>') !!}

<br><br>Város *:
<input type="text" name="city" value="{{$restaurant->city}}">
{!! $errors->first('city', '<p class="help-block">:message</p>') !!}

<br><br>Típus *:
<select name="category_id">
    @foreach ($categories as $category)
        <option value="{{$category->id}}"
        @if($restaurant->category->id == $category->id) 
        {{'selected'}}
        @endif
        >{{$category->name}}</option>
    @endforeach
</select>
{!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}

<br><br>Megye *:
<select name="county">
     <option >Bács-Kiskun megye</option>
    <option >Baranya megye</option>
    <option >Békés megye</option>
    <option >Borsod-Abaúj-Zemplén megye</option>
    <option >Csongrád megye</option>
    <option >Fejér megye</option>
    <option >Győr-Moson-Sopron megye</option>
    <option>Hajdú-Bihar megye</option>
    <option >Heves megye</option>
    <option >Jász-Nagykun-Szolnok megye</option>
    <option >Komárom-Esztergom megye</option>
    <option >Nógrád megye</option>
    <option >Pest megye</option>
    <option >Somogy megye</option>
    <option >Szabolcs-Szatmár-Bereg megye</option>
    <option >Tolna megye</option>
    <option >Vas megye</option>
    <option >Veszprém megye</option>
    <option >Zala megye</option>                                               
</select>
{!! $errors->first('county', '<p class="help-block">:message</p>') !!}



<br><br>Új Kép feltöltése:
<input type="file" name="image">
{!! $errors->first('image', '<p class="help-block">:message</p>') !!}

<br><br>Kiemelt: 
<select name="emphasized">
        <option value="0"
        @if($restaurant->emphasized == false) 
        {{'selected'}}
        @endif>Nem</option>
        <option value="1"
        @if($restaurant->emphasized) 
        {{'selected'}}
        @endif>Igen</option>
</select><br>

<br><br>Új Galéria feltöltése:
{!! $errors->first('gallery[]', '<p class="help-block">:message</p>') !!}
<input type="file" name="newGallery[]">
<input type="file" name="newGallery[]">
<input type="file" name="newGallery[]">
<input type="file" name="newGallery[]">
<br>
@if($restaurant->gallery)
Jelenlegi galéria képek megtartása/elvetése:<br>
@foreach($restaurant->gallery as $image)
    <label><input type="checkbox" name="oldGallery[]" value="{{$image}}" checked> {{$image}}</label><br><img style="width: 70px" src="/storage/restaurants/{{$image}}">
    <br>
  @endforeach
@endif
  <br>

<br><br>Leírás:
<textarea class="textarea" name="description">{!! old('description') ?: $restaurant->description!!}</textarea>

<br><br>Étel 1:
Név:
<input type="text" name="food_one" value="{{ old('food_one') ?: $restaurant->food_one}}">
Részletek:
<textarea rows="3" cols="20" name="food_one_desc">{!! old('food_one_desc') ?: $restaurant->food_one_desc !!}</textarea>

Ár:
<input type="number" name="food_one_price" value="{{ old('food_one_price') ?: $restaurant->food_one_price}}"> Ft


<br><br>Étel 2:
Név:
<input type="text" name="food_two" value="{{ old('food_two') ?: $restaurant->food_two}}">
Részletek:
<textarea rows="3" cols="20" name="food_two_desc">{!! old('food_two_desc') ?: $restaurant->food_two_desc !!}</textarea>
Ár:
<input type="number" name="food_two_price" value="{{ old('food_two_price') ?: $restaurant->food_two_price}}"> Ft

<br><br>Étel 3:
Név:
<input type="text" name="food_three" value="{{ old('food_three') ?: $restaurant->food_three}}">
Részletek:
<textarea rows="3" cols="20" name="food_three_desc">{!! old('food_three_desc') ?: $restaurant->food_three_desc !!}</textarea>

Ár:
<input type="number" name="food_three_price" value="{{ old('food_three_price') ?: $restaurant->food_three_price}}"> Ft

<br><br>Étel 4:
Név:
<input type="text" name="food_four" value="{{ old('food_four') ?: $restaurant->food_four}}">
Részletek:
<textarea rows="3" cols="20" name="food_four_desc">{!! old('food_four_desc') ?: $restaurant->food_four_desc !!}</textarea>

Ár:
<input type="number" name="food_four_price" value="{{ old('food_four_price') ?: $restaurant->food_four_price}}"> Ft
{!! $errors->first('food_four_price', '<p class="help-block">:message</p>') !!}


<br><br><h3>Értékelések:</h3>

<br><br><h4>Étel:</h4>
Szöveges értékelés:
<textarea class="textarea" name="food_rate_desc" >{!! old('food_rate_desc') ?: $restaurant->food_rate_desc !!}</textarea><br>
Pontszám*:
<input type="number" name="food_rate" value="{{ old('food_rate') ?: $restaurant->food_rate}}">
{!! $errors->first('food_rate', '<p class="help-block">:message</p>') !!}


<br><br><h4>Felszolgálás:</h4>
Szöveges értékelés:
<textarea class="textarea" name="service_rate_desc">{!! old('service_rate_desc') ?: $restaurant->service_rate_desc !!}</textarea><br>
Pontszám*:
<input type="number" name="service_rate" value="{{ old('service_rate') ?: $restaurant->service_rate}}">
{!! $errors->first('service_rate', '<p class="help-block">:message</p>') !!}


<br><br><h4>Étterem:</h4>
Szöveges értékelés:
<textarea class="textarea" name="place_rate_desc" >{!! old('place_rate_desc') ?: $restaurant->place_rate_desc !!}</textarea><br>
Pontszám*:
<input type="number" name="place_rate" value="{{ old('place_rate') ?: $restaurant->place_rate}}">
{!! $errors->first('place_rate', '<p class="help-block">:message</p>') !!}


<br><br>Cím:
<input type="text" name="address" value="{{ old('address') ?: $restaurant->address}}">

<br><br>URL:
<input type="text" name="url" value="{{ old('url') ?: $restaurant->url}}">

<br><br><h3>Jellemzők:</h3><br>
@foreach($features as $feature)
    <label><input type="checkbox" name="features[]" value="{{$feature->id}}"
     {{$restaurant->hasFeature($feature->id) ? 'checked' : ''}}
    > {{$feature->name}}</label>
    <br>
  @endforeach

<br><br><input type="submit" value="Módosítás">

</form>
</div>


@endsection