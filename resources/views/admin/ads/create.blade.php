@extends('admin.layouts.template')

@section('content')

<h1>Hirdetés hozzáadása</h1>

@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
@if(Session::has('error-msg'))
<p class="alert alert-danger">{{ Session::get('error-msg') }}</p>
@endif

<form action="{{route('ad.store')}}" method="POST" enctype="multipart/form-data">
@csrf

URL *:
<input type="text" name="url" value="{{ old('url') }}">
{!! $errors->first('url', '<p class="help-block">:message</p>') !!}
<br><br>

Cím *:
<input type="text" name="title" value="{{ old('title') }}">
{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
<br><br>

Alcím:
<input type="text" name="sub_title" value="{{ old('sub_title') }}"><br><br>
Kép*:
<input type="file" name="image">
{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
<br><br>

<input type="submit" value="Hozzáadás">

</form>
<br>


@endsection