<?php

use Illuminate\Database\Seeder;
use App\Models\Feature;
class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f_one = new Feature;
        $f_one->name = "Bankkártyás fizetés";
        $f_one->save();
        $f_two = new Feature;
        $f_two->name = "Kerékpár parkolás";
        $f_two->save();
        $f_three = new Feature;
        $f_three->name = "WIFI";
        $f_three->save();
        $f_four = new Feature;
        $f_four->name = "Foglalás";
        $f_four->save();
        $f_five = new Feature;
        $f_five->name = "Parkoló";
        $f_five->save();
        $f_six = new Feature;
        $f_six->name = "Dohányzó";
        $f_six->save();
        $f_seven = new Feature;
        $f_seven->name = "Akadálymentesített";
        $f_seven->save();
        $f_eight = new Feature;
        $f_eight->name = "Kupon";
        $f_eight->save();
        $f_nine = new Feature;
        $f_nine->name = "Állatbarát";
        $f_nine->save();
        
    }
}
