<?php

namespace App\Users\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function isDefault(){
        return $this->role_id == Role::SUPERADMIN;
    }
    public function isDefaultSuperAdmin(){
        return $this->id == 1;
    }
    public function setAttributes($params){
        $this->name = $params['name'];
        $this->email = $params['email'];
        $this->role()->associate($params['role_id']);
        if(isset($params['password'])){
            $this->password = \Hash::make($params['password']);
        }
    }
    
    public function hasPermission($permId){
        return $this->role->hasPermission($permId);
    }
    public function sendPasswordResetNotification($token){
        $this->notify(new AdminResetPasswordNotification($token));
    }
}
