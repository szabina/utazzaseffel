<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_restaurant', function (Blueprint $table) {
            $table->timestamps();
            $table->unsignedBigInteger('restaurant_id');
            $table->unsignedBigInteger('feature_id');
            $table->unique(['restaurant_id', 'feature_id']);
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('feature_id')->references('id')->on('features');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_restaurant');
    }
}
