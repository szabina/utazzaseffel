<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSlugs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nemzetkozi = Category::find(1);
        $nemzetkozi->slug_name = 'nemzetkozi-es-streetfood';
        $nemzetkozi->save();

        $modern = Category::find(2);
        $modern->slug_name = 'modern-fine-dining-ettermek';
        $modern->save();

        $magyar = Category::find(3);
        $magyar->slug_name = 'magyaros-ettermek';
        $magyar->save();

        $cukraszda = Category::find(4);
        $cukraszda->slug_name = 'cukraszda';
        $cukraszda->save();
    }
}
