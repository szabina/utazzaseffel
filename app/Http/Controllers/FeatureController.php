<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Feature;

class FeatureController extends Controller
{

    public function list(){
        $features = Feature::all();
        return view('admin.features.list')->with('features', $features);
    }

    public function create(){
        return view('admin.features.create');
    }
    public function store(Request $req){
        Validator::make($req->all(), [
            'name' => 'required',
        ])->validate();

        $feature = new Feature;
        $feature->name = $req->input('name');
        $feature->save();
        \Session::flash('message', 'Sikeres mentés!'); 
        return redirect()->back();
    }
    public function delete (string $id){
        $feature = Feature::find($id);

        //Ha tartozik hozzá étterem, akkor automatikusan töröljük a velük való kapcsolatot
        $restaurants = $feature->restaurants;
        if(count($restaurants) > 0){
            foreach ($restaurants as $restaurant) {
                $restaurant->features()->detach($feature->id);
            }
        }
        //Alap jellemzők ne legyenek törölhetők
        if($feature->isDefault()){
            return redirect()->back();
        };
        $feature->delete();
        \Session::flash('message', 'Sikeres törlés!'); 
        return redirect()->back();
    }
}
