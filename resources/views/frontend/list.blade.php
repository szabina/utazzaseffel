
    @extends('frontend.inc')

@section('content')

    <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('img/hero2.jpg')}})">
    </div>
    <!-- ***** Breadcumb Area End ***** -->
    <div class="container-fluid">
        <!-- Explore Area -->
        <section class="dorne-explore-area row">
            <!-- Explore Search Area -->


            <!-- Explore Search Form -->
            
                        
                        @include('frontend.sideSearch')

                        
                        

                    </div>
                </div>
            </div>

            <div class="col order-1 order-md-2 pt-2 pt-md-4">
                <div class="row mt-1 mt-md-3">
                    <div class="category-list-about">
                        <h2 class="mb-4">{{$category->name}}</h2>
                        <p class="mb-4">{{$category->description}}</p>
                    </div>

                    
                @foreach ($restaurants as $restaurant)
                    <div class=" row single-feature-events-area wow   animated animated" data-wow-delay="0.6s"
                        style="visibility: visible; animation-delay: 0.6s;">
                        <div class="feature-events-thumb feature-events-thumb-c col-5 customcol5">
                            <a href="{{route('restaurant.show', ['category_slug' => $restaurant->getCategorySlug(), 'restaurant_slug' => $restaurant->slug])}}">
                                <img src="/storage/restaurants/{{$restaurant->image}}" alt="" class="img-fluid">
                            </a>

                        </div>
                        <div
                            class="feature-events-content col-7 col-sm-7 col-xs-7 d-flex-column justify-content-around u-no-padding">

                            <div class="kartyaszoveg">
                                    
                                        <a href="{{route('restaurant.show', ['category_slug' => $restaurant->getCategorySlug(), 'restaurant_slug' => $restaurant->slug])}}">
                                    <h5>
                                        {{$restaurant->name}} |
                                    </h5>
                                        </a>
                                    
                                </a>
                                <h5>{{$restaurant->city}}
                                </h5>
                                <h6>{{$restaurant->city}}</h6>
                                <h6>Összbenyomás {{$restaurant->overall_rate}}p</h6>
                                <p>Étel {{$restaurant->food_rate}}p | </p>
                                <p>Felszolgálás {{$restaurant->service_rate}}p | </p>
                                <p>Étterem {{$restaurant->place_rate}}p</p>
                            </div>


                            <div class="row checkboxrow">
                                @foreach ($features as $feature)
                                        <div class="col-6">

                                        <input type="checkbox" id="{{$feature->name}}" 
                                        {{$restaurant->hasFeature($feature->id) ? 'checked' : ''}}
                                        disabled>

                                        <label for="{{$feature->name}}">{{$feature->name}}</label>
                                        </div>
                                @endforeach
                                                

                                

                            </div>

                            <div class="feature-events-details-btn">
                                <a href="{{route('restaurant.show', ['category_slug' => $restaurant->getCategorySlug(), 'restaurant_slug' => $restaurant->slug])}}">→</a>
                            </div>
                        </div>
                    </div>
                @endforeach   
                    
                    
                      

                    




                <!-- <div class="card">
                        <div class="card-horizontal">
                            <div class="img-square-wrapper">
                                <img class="" src="http://via.placeholder.com/300x180" alt="Card image cap">
                            </div>
                            <div class="card-body d-flex-column justify-content-evenly">
                                <h4 class="card-title">Card title <a href="#">| Nemzetközi |</a></h4>
                                <a href="#">| Magyaros |</a>
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.
                                <span class="gombtarto"><a href="" class="kartyagomb">Tovább</a></span>
                                </p>
                                    
                            </div>
                        </div>
    
                    -->





        </section>
    </div>




    @endsection