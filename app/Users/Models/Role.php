<?php

namespace App\Users\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const SUPERADMIN = 1;

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
    public function admins(){
        return $this->hasMany(Admin::class);
    }
    public function hasPermission($permissionId){
        return in_array($permissionId, $this->permissions()->pluck('id')->toArray());
    }
    
    public function permissionsList(){
        return $this->permissions()->pluck('name')->toArray();
    }
}
