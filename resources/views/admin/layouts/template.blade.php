<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Utazz a séffel | Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="{{asset('backend/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="{{asset('backend/css/style.css')}}" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="{{asset('backend/css/font-awesome.css')}}" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='{{asset('backend/css/SidebarNav.min.css')}}' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="{{asset('backend/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('backend/js/modernizr.custom.js')}}"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="{{asset('backend/js/metisMenu.min.js')}}"></script>
<script src="{{asset('backend/js/custom.js')}}"></script>
<link href="{{asset('backend/css/custom.css')}}" rel="stylesheet">
<!--//Metis Menu -->

</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="{{route('homepage')}}"><span><img src="{{asset('img/icons/favicon-32x32.png')}}"></span> Utazz a séffel</a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">MENÜ</li>
              
@if(auth()->guard('admin')->user()->hasPermission(\App\Users\Models\Permission::EDIT_CONTENT))
			  <li class="treeview">
                <a href="#">
                <span>Éttermek</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{route('restaurant.create')}}"> Új étterem létrehozása</a></li>
                  <li><a href="{{route('restaurants.list')}}"> Éttermek kezelése</a></li>
                  <li><a href="{{route('restaurant.selectEmphasized')}}"> Kiemelt éttermek választása</a></li>

                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                <span>Hirdetések</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{route('ad.create')}}"> Új hirdetés létrehozása</a></li>
                  <li><a href="{{route('ads.list')}}"> Hirdetések kezelése</a></li>
                </ul>
              </li>
              
               <li class="treeview">
                <a href="#">
                <span>Kategóriák</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{route('category.create')}}"> Új kategória létrehozása</a></li>
                  <li><a href="{{route('categories.list')}}"> Kategóriák kezelése</a></li>
                </ul>
              </li>

              {{-- <li class="treeview">
                <a href="#">
                <span>Jellemzők</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{route('feature.create')}}"> Új jellemző létrehozása</a></li>
                  <li><a href="{{route('features.list')}}"> Jellemzők listája</a></li>
                </ul>
              </li> --}}

              <li class="treeview"><a href="{{route('messages.list')}}"> Üzenetek</a></li>
 @endif             
              <li class="treeview"><a href="{{route('admin.profile', ['admin_id' => auth()->guard('admin')->user()->id])}}"> Adataim </a><li>

            @if(auth()->guard('admin')->user()->hasPermission(\App\Users\Models\Permission::EDIT_USERS))
                <li class="treeview"><a href="{{route('admin.create', ['admin_id' => auth()->guard('admin')->user()->id])}}"> Admin hozzáadása </a><li>
            @endif

            <li class="treeview"><a href="{{route('admin.list', ['admin_id' => auth()->guard('admin')->user()->id])}}"> Adminok listázása </a><li>
            
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class=" header-section ">
			<div class="header-left">
				
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				
				
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
								<div class="profile_img">	
									
									<div class="user-name">
										<p>{{auth()->guard('admin')->user()->name}}</p>
										<span>{{auth()->guard('admin')->user()->role->name}}</span>
									</div>
									
									<div class="clearfix"></div>	
								</div>	
							</li>
 
								<li>
                                <form action="{{route('admin.logout')}}" method="POST"  autocomplete="off">
                                    {{csrf_field()}}

        
                                    <input type="hidden" name="_method" value="DELETE">
          
        
                                    <input type="submit" class="btn " value="Kilépés">
                                </form><br>
                                </li>
							
						
					</ul>
				</div>
								
			</div>
		
		</div>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			
				
				<div class=" scroll" id="">
					    @yield('content')

				
			</div>
		</div>
		<!--footer-->
		
        <!--//footer-->
	</div>
	{{-- disable scroll in input type number --}}
<script>
$(function(){

  $(':input[type=number]').on('mousewheel',function(e){ $(this).blur(); });

});
</script>
	<!-- side nav js -->
	<script src='{{asset('backend/js/SidebarNav.min.js')}}' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="{{asset('backend/js/classie.js')}}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="{{asset('backend/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('backend/js/scripts.js')}}"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="{{asset('backend/js/bootstrap.js')}}"> </script>
   <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    let textareas = document.querySelectorAll('.textarea');
    if(textareas){
    textareas.forEach((textarea)=> {
      CKEDITOR.replace( textarea );
    });
    }
</script>
<script>
let deleteLinks = document.querySelectorAll('.delete');

for (let i = 0; i < deleteLinks.length; i++) {
    deleteLinks[i].addEventListener('submit', function(event) {
        
        if(!confirm("Biztos a törlésben?")){
          event.preventDefault();
        } else {
          return true;
        }
    });
}
</script>
   
</body>


</html>