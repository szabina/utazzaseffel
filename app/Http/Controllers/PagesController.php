<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Restaurant;

class PagesController extends Controller
{
    //homepage betöltése a kiemelt éttermekkel, hirdetésekkel, valamint a kategóriákkal
    public function index(){
        $emphasized = Restaurant::where('emphasized', true)->orderBy('created_at', 'desc')->get();
        $ads = Ad::all();
        $categories = Category::all();
        return view('frontend.index')->with('ads', $ads)->with('categories', $categories)->with('emphasized', $emphasized);
    }

    public function faq(){
        return view('frontend.faq');
    }

}
