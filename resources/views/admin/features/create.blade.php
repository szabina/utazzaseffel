@extends('admin.layouts.template')

@section('content')

<h1>Jellemző hozzáadása</h1>

<form action="{{route('feature.store')}}" method="POST">
@csrf
Név:
<input type="text" name="name">
{!! $errors->first('name', '<p class="help-block">:message</p>') !!}

<input type="submit" value="Hozzáadás">

</form><br>
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif

@endsection