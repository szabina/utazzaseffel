<!-- Explore Search Form -->
            <div class=" explore-search-form col-md-4 order-2 order-md-1">
                <h6>Milyen éttermet keresel?</h6>
                <!-- Tabs -->
            @if(count($ads) > 0)
                @foreach ($ads as $index => $ad) 
                    @if($index == 0)
                    <div style="border:none" class="nav nav-tabs d-flex flex-column justify-content-center"  role="tablist">
                    {{-- <h3 class="hirdetes-title">{{$ad->title}} --}}
                  </h3>
                    <a href="{{$ad->url}}" target="_blank"><img style="width:100%; height: 290px; object-fit:cover" src="/storage/restaurants/{{$ad->image}}"  alt="{{$ad->title}}"></a>
                    <!--PLACEHOLDER-->
                    
                  <p class="hirdetes-subtitle">{{$ad->title}} | {{$ad->sub_title}} |<span> Hirdetés</span></p>
                </div>
                <!-- Tabs Content -->
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-places" role="tabpanel"
                        aria-labelledby="nav-places-tab">

                        <form action="{{route('restaurants.search')}}" method="get" class="d-flex-column align-items-center justify-content-center">


                                @foreach ($categories as $category)
                                   <input type="checkbox" id="{{$category->id}}" name="multiple_category[]" value="{{$category->id}}">
                                    <label for="{{$category->id}}">{{$category->name}}</label>
                                @endforeach
                                

                                
                                

    

                                <h3>Megye</h3>

                                <select class="custom-select" name="search[county]">

                                    <option value="" disabled selected>Kiválasztás</option>
                                    

                                    <option >Bács-Kiskun megye</option>
                                    <option>Baranya megye</option>
                                    <option >Békés megye</option>
                                    <option >Borsod-Abaúj-Zemplén megye</option>
                                    <option >Csongrád megye</option>
                                    <option>Fejér megye</option>
                                    <option >Győr-Moson-Sopron megye</option>
                                    <option >Hajdú-Bihar megye</option>
                                    <option >Heves megye</option>
                                    <option >Jász-Nagykun-Szolnok megye</option>
                                    <option>Komárom-Esztergom megye</option>
                                    <option >Nógrád megye</option>
                                    <option >Pest megye</option>
                                    <option >Somogy megye</option>
                                    <option >Szabolcs-Szatmár-Bereg megye</option>
                                    <option >Tolna megye</option>
                                    <option >Vas megye</option>
                                    <option >Veszprém megye</option>
                                    <option >Zala megye</option>

                                </select>

                                <h3>Összbenyomás</h3>

                                <select class="custom-select" name="search[rate]">

                                    <option value="" disabled selected>Értékelés</option>
                                    

                                    <option value="10">10</option>

                                    <option value="9">9</option>

                                    <option value="8">8</option>

                                    <option value="7">7</option>

                                    <option value="6">6</option>

                                    <option value="5">5</option>

                                    <option value="4">4</option>

                                    <option value="3">3</option>

                                    <option value="2">2</option>

                                    <option value="1">1</option>



                                </select>

    

    

                                <div class="row mt-md-5 mt-0">

                                    <div class="col-12  d-flex justify-content-center">

                                        <button type="submit" class="btn dorne-btn   bg-white text-dark"><i

                                                class="fa fa-search pr-2" aria-hidden="true"></i> Keresés</button>

                                    </div>

    

    

    

    

    

                                </div>

    

    

                            </form>

                            @else
                    <br><br>
                    <div style="border:none" class="nav nav-tabs d-flex flex-column align-items-center justify-content-center" role="tablist">
                    {{-- <h3 class="hirdetes-title">{{$ad->title}} --}}
                  </h3>
                    <a href="{{$ad->url}}" target="_blank"><img style="width:100%; height: 290px; object-fit:cover" src="/storage/restaurants/{{$ad->image}}"  alt="{{$ad->title}}"></a>
                    <!--PLACEHOLDER-->
                    
                  <p class="hirdetes-subtitle">{{$ad->title}} | {{$ad->sub_title}} |<span> Hirdetés</span></p>
                </div>
                    <br>

                @endif
                @endforeach

            @else
            
            
                    
                <!-- Tabs Content -->
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-places" role="tabpanel"
                        aria-labelledby="nav-places-tab">

                        <form action="{{route('restaurants.search')}}" method="get" class="d-flex-column align-items-center justify-content-center">


                                @foreach ($categories as $category)
                                   <input type="checkbox" id="{{$category->id}}" name="multiple_category[]" value="{{$category->id}}">
                                    <label for="{{$category->id}}">{{$category->name}}</label>
                                @endforeach
                                

                                
                                

    

                                <h3>Megye</h3>

                                <select class="custom-select" name="search[county]">

                                    <option value="" disabled selected>Kiválasztás</option>
                                    

                                    <option >Bács-Kiskun megye</option>
                                    <option>Baranya megye</option>
                                    <option >Békés megye</option>
                                    <option >Borsod-Abaúj-Zemplén megye</option>
                                    <option >Csongrád megye</option>
                                    <option>Fejér megye</option>
                                    <option >Győr-Moson-Sopron megye</option>
                                    <option >Hajdú-Bihar megye</option>
                                    <option >Heves megye</option>
                                    <option >Jász-Nagykun-Szolnok megye</option>
                                    <option>Komárom-Esztergom megye</option>
                                    <option >Nógrád megye</option>
                                    <option >Pest megye</option>
                                    <option >Somogy megye</option>
                                    <option >Szabolcs-Szatmár-Bereg megye</option>
                                    <option >Tolna megye</option>
                                    <option >Vas megye</option>
                                    <option >Veszprém megye</option>
                                    <option >Zala megye</option>

                                </select>

                                <h3>Összbenyomás</h3>

                                <select class="custom-select" name="search[rate]">

                                    <option value="" disabled selected>Értékelés</option>
                                    

                                    <option value="10">10</option>

                                    <option value="9">9</option>

                                    <option value="8">8</option>

                                    <option value="7">7</option>

                                    <option value="6">6</option>

                                    <option value="5">5</option>

                                    <option value="4">4</option>

                                    <option value="3">3</option>

                                    <option value="2">2</option>

                                    <option value="1">1</option>



                                </select>

    

    

                                <div class="row mt-md-5 mt-0">

                                    <div class="col-12  d-flex justify-content-center">

                                        <button type="submit" class="btn dorne-btn   bg-white text-dark"><i

                                                class="fa fa-search pr-2" aria-hidden="true"></i> Keresés</button>

                                    </div>

    

    

    

    

    

                                </div>

    

    

                            </form>
                           
                  @endif
                