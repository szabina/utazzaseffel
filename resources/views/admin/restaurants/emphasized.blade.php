@extends('admin.layouts.template')

@section('content')
 <h1>Kiemelt éttermek</h1>
 <p>Válassza ki, melyik éttermeket szeretné a főoldalon, a Séf ajánlata blokkban szerepeltetni</p><br>
 @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif 

<form method="post" action="{{route('restaurant.saveEmphasized')}}">
{!! $errors->first('emphasized', '<p class="help-block">:message</p>') !!}
@csrf
@foreach ($restaurants as $restaurant)
        <div>

        <input type="checkbox" id="{{$restaurant->name}}" name="emphasized[]" value="{{$restaurant->id}}"
        @if($restaurant->emphasized == '1')
        {{'checked'}}
        @endif
        >{{$restaurant->name}}, {{$restaurant->city}}

        </div>
@endforeach
<input type="submit" value="Mentés">
</form>

@endsection