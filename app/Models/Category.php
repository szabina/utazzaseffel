<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //A 4 alap kategória
    protected $defaults = ['1', '2', '3', '4'];

    public function restaurants(){
        return $this->hasMany(Restaurant::class);
    }
    public function isDefault(){
        return in_array($this->id, $this->defaults); 
    }
    
}
