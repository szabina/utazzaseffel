<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    //A 9 alap jellemző
    public $defaults = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

    public function restaurants(){
        return $this->belongsToMany(Restaurant::class);
    }

    public function isDefault(){
        return in_array($this->id, $this->defaults);
    }

}
