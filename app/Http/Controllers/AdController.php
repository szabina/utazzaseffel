<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;
use Illuminate\Support\Facades\Validator;


class AdController extends Controller
{
    
    public function list(){
        $ads = Ad::orderBy('title', 'asc')->get();
        return view('admin.ads.list')->with('ads', $ads);
    }

    public function create(){
        return view('admin.ads.create');
    }
    public function store(Request $req){

        $ad = new Ad;
        $ad->setAttributes($req, $ad);
        $ad->save();
        \Session::flash('message', 'Sikeres mentés!'); 
        return redirect()->route('ads.list');

    }
    public function edit(string $id){
        $ad = Ad::find($id);
        return view('admin.ads.edit')->with('ad', $ad);
    }
    public function update($id, Request $req){
        $validator = Validator::make($req->all(), [
            'title' => 'required',
            'url' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            \Session::flash('error-msg', 'Feltöltés nem sikerült!');
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        };

        $ad = Ad::find($id);
        $ad->setAttributes($req, $ad);
        $ad->save();
        \Session::flash('message', 'Sikeres módosítás!'); 
        return redirect()->route('ads.list');

    }
    public function delete (string $id){
        $ad = Ad::find($id);
        $ad->delete();
        \Session::flash('message', 'Sikeres törlés!'); 
        return redirect()->back();
    }
}
