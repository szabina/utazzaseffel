<?php

use Illuminate\Database\Seeder;
use App\Users\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission;
        $permission->id = Permission::SEE_PERMISSIONS;
        $permission->name = 'See permissions';
        $permission->save();
        $permission = new Permission;
        $permission->id = Permission::EDIT_PERMISSIONS;
        $permission->name = 'Edit permissions';
        $permission->save();
        $permission = new Permission;
        $permission->id = Permission::SEE_USERS;
        $permission->name = 'See users';
        $permission->save();
        $permission = new Permission;
        $permission->id = Permission::EDIT_USERS;
        $permission->name = 'Edit users';
        $permission->save();
        $permission = new Permission;
        $permission->id = Permission::SEE_ROLES;
        $permission->name = 'See roles';
        $permission->save();
        $permission = new Permission;
        $permission->id = Permission::EDIT_ROLES;
        $permission->name = 'Edit roles';
        $permission->save();
        $permission->id = Permission::EDIT_CONTENT;
        $permission->name = 'Edit content';
        $permission->save();
    }
}
