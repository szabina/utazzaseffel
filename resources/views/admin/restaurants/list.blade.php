 @extends('admin.layouts.template')

@section('content')
<form action="{{route('restaurants.list')}}" method="get">
  @csrf
      <input type="search" name="top_search" id="search"
      placeholder="Keresés...">
      <input type="submit" class="btn" value="Keresés">
</form>
<br>
<form action="{{route('restaurants.list')}}" method="get">
@csrf
          <label for="hero-search-input-1">Típus</label>
                  <select class="custom-select" name="search[category]" id="hero-search-input-1">
                        <option></option>
                      @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                  </select>                              
            <label for="hero-search-input-2">Megye</label>
                                                <select class="custom-select" id="hero-search-input-2" name="search[county]">
                                                    <option></option>
                                                    <option >Bács-Kiskun megye</option>
                                                    <option >Baranya megye</option>
                                                    <option >Békés megye</option>
                                                    <option >Borsod-Abaúj-Zemplén megye</option>
                                                    <option >Csongrád megye</option>
                                                    <option >Fejér megye</option>
                                                    <option >Győr-Moson-Sopron megye</option>
                                                    <option >Hajdú-Bihar megye</option>
                                                    <option >Heves megye</option>
                                                    <option >Jász-Nagykun-Szolnok megye</option>
                                                    <option >Komárom-Esztergom megye</option>
                                                    <option >Nógrád megye</option>
                                                    <option >Pest megye</option>
                                                    <option >Somogy megye</option>
                                                    <option >Szabolcs-Szatmár-Bereg megye</option>
                                                    <option >Tolna megye</option>
                                                    <option >Vas megye</option>
                                                    <option >Veszprém megye</option>
                                                    <option >Zala megye</option>
                                                </select>                                               

     <label for="hero-search-input-3">Összbenyomás</label>
                                                <select class="custom-select" id="hero-search-input-3" name="search[rate]">
                                                    <option></option>
                                                    <option value="10">10</option>
                                                    <option value="9">9</option>
                                                    <option value="8">8</option>
                                                    <option value="7">7</option>
                                                    <option value="6">6</option>
                                                    <option value="5">5</option>
                                                    <option value="4">4</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                </select>  

    <button type="submit"
                                                    class="btn dorne-btn search-hero-dorne-btn mt-auto"><i
                                                        class="fa fa-search pr-2" aria-hidden="true"></i>
                                                    Keresés</button>                                     
</form><br>


 <h1>Éttermek ({{count($restaurants)}})</h1>
 <h2>Rendezés:</h2>
 <h4><a href="{{route('restaurants.list', ['search' =>
        [
          'order_by' => 'name',
          'sort_dir' => request()->input('search.order_by') == 'name' && request()->input('search.sort_dir') == 'asc' ? 'desc' : 'asc'
        ]
      ])}}">Név szerint</a></h4>

<h4><a href="{{route('restaurants.list', ['search' =>
        [
          'order_by' => 'updated_at',
          'sort_dir' => request()->input('search.order_by') == 'updated_at' && request()->input('search.sort_dir') == 'asc' ? 'desc' : 'asc'
        ]
      ])}}">Legújabbak</a></h4>

      <br>




 @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif 

@foreach ($restaurants as $restaurant)
<div style="display:flex; flex-direction:row; justify-content: space-between; border-bottom: 1px solid grey; height: 150px; padding:10px">
            <img style="width: 150px;" src="/storage/restaurants/{{$restaurant->image}}">

            <div style="width: 230px">Név: {{$restaurant->name}}<br>
            Kategória: {{$restaurant->category->name}}<br>
            Megye: {{$restaurant->county}}<br>

            </div>
            <div>
              <a style="margin-bottom:5px" class="btn btn-success" href="{{route('restaurant.edit', ['restaurant_id' => $restaurant->id])}}">Módosítás</a>
              <form class="delete" action="{{route('restaurant.delete', ['restaurant_id' => $restaurant->id])}}" method="POST">
              @csrf
        
              <input type="hidden" name="_method" value="DELETE">        
              <button type="submit" class="btn btn-danger">Törlés</button>
             </form>
          </div>
          
    
</div>
<br>
@endforeach

@endsection