<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    public function saveImage($req){
        //Handle file upload
        if($req->hasFile('image')){
            //Get filename with extension
            $fullFilename = $req->file('image')->getClientOriginalName();
            //Get just file name
            $filename = pathinfo($fullFilename, PATHINFO_FILENAME );
            //Get just extension
            $ext = $req->file('image')->getClientOriginalExtension();

            $filenameToStore = $filename.'_'.time().'.'.$ext;

            $path = $req->file('image')->storeAs('public/restaurants', $filenameToStore);
            return $filenameToStore;

        };
    }


    public function setAttributes($req, $ad){
        $ad->title = $req->input('title');
        $ad->sub_title = $req->input('sub_title');
        $ad->url = $this->urlCorrection($req->input('url') ?: '');
        if($req->hasFile('image')){
            $ad->image = $this->saveImage($req);
        }
        return $ad;
    }
    public function urlCorrection(string $inputUrl){
        if (strpos($inputUrl, 'http') !== false) {
            return $inputUrl;
        }
        else {
            return '//'.$inputUrl;
        }
    }
}
