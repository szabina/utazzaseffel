<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('restaurants', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name');
      $table->string('city');
      $table->unsignedBigInteger('category_id')->nullable();

      $table->string('county');
      $table->integer('overall_rate')->nullable();
      $table->string('image');
      $table->boolean('emphasized')->nullable();
      $table->text('gallery')->nullable();
      $table->longText('description')->nullable();
      $table->string('food_one')->nullable();
      $table->string('food_one_desc')->nullable();
      $table->integer('food_one_price')->nullable();
      $table->string('food_two')->nullable();
      $table->string('food_two_desc')->nullable();
      $table->integer('food_two_price')->nullable();
      $table->string('food_three')->nullable();
      $table->string('food_three_desc')->nullable();
      $table->integer('food_three_price')->nullable();
      $table->string('food_four')->nullable();
      $table->string('food_four_desc')->nullable();
      $table->integer('food_four_price')->nullable();
      $table->longText('food_rate_desc')->nullable();
      $table->integer('food_rate')->nullable();
      $table->longText('service_rate_desc')->nullable();
      $table->integer('service_rate')->nullable();
      $table->longText('place_rate_desc')->nullable();
      $table->integer('place_rate')->nullable();
      $table->string('address')->nullable();
      $table->string('url')->nullable();

      $table->timestamps();

      $table->foreign('category_id')->references('id')->on('categories');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('restaurants');
  }
}
