@extends('admin.layouts.template')

@section('content')

<h1>Üzenetek</h1>
<form method="GET">
      <input type="hidden" name="unresolved" value="true" >
      <br><input class="btn btn-success" type="submit" value="Csak az elintézendőket mutasd!">
    </form><br>

@foreach ($messages as $message)
    <ul>
        <li>Érkezett: {{$message->created_at}}<br>
        Email: {{$message->email}}<br>
        Név: {{$message->name}}<br>
        Elintézve: 
        @if($message->resolved)
        {{'Igen'}}
        @else
        {{'Nem'}}
        @endif
        </li>
        <button class="btn"><a href="{{route('message.show', ['message_id' => $message->id ])}}">Elolvas</a></button>
          
    </ul><br>
@endforeach

@endsection