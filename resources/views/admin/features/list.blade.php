@extends('admin.layouts.template')

@section('content')
 <h1>Jellemzők</h1>
    <p>A kilenc alap jellemző mellé lehetősége van újabbat felvenni az Új jellemző létrehozása menüpontban.</p><br>

 @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif 

@foreach ($features as $feature)
    <ul>
        <li>{{$feature->name}}</li>
        @if(!$feature->isDefault())
          
            <form class="delete" action="{{route('feature.delete', ['feature_id' => $feature->id])}}" method="POST">
            @csrf
        
            <input type="hidden" name="_method" value="DELETE">        
            <input type="submit" class="btn btn-danger" value="Törlés">
            </form>
          
        @endif  
    </ul>
@endforeach

@endsection