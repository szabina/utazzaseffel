 @extends('admin.layouts.template')

@section('content')
 <h1>Kategóriák</h1>
    <p>A négy alap kategória mellé lehetősége van újabb kategóriát (pl.egyéb) hozzáadni, mely szerepelni fog az oldal típusok közti keresőjében.</p><br>

 @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif 
    @if(Session::has('alert'))
        <p class="alert alert-danger">{{ Session::get('alert') }}</p>
    @endif 

@foreach ($categories as $category)
    <ul>
        <li>{{$category->name}}</li>
        @if(!$category->isDefault())
          
            <form class="delete" action="{{route('category.delete', ['cat_id' => $category->id])}}" method="POST">
            @csrf
        
            <input type="hidden" name="_method" value="DELETE">        
            <input type="submit" class="btn btn-danger" value="Törlés">
            </form>
          
        @endif  
    </ul>
@endforeach

@endsection