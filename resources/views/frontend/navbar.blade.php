 <!-- ***** Search Form Area ***** -->
    <div class="dorne-search-form d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-close-btn" id="closeBtn">
                        <i class="pe-7s-close-circle" aria-hidden="true"></i>
                    </div>
                    <form action="{{route('restaurants.search')}}" method="get">
                    @csrf
                        <input type="search" name="top_search" id="search"
                            placeholder="Keresés éttermek, helyszínek, ételek, leírások közt...">
                        <input type="submit" class="d-none" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area" id="header">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg">
                        <a class="navbar-brand" href="{{route('homepage')}}">
                            Utazz a Séffel
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dorneNav"
                            aria-controls="dorneNav" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="fa fa-bars" aria-hidden="true"></span></button>
                        <!-- Nav -->
                        <div class="collapse navbar-collapse" id="dorneNav">
                            <ul class="navbar-nav mr-auto" id="dorneMenu">
                                <li class="nav-item 
                                @if(Route::current()->getName() == 'homepage')
                                        {{'active'}}
                                @endif
                                ">
                                    <a class="nav-link" href="{{route('homepage')}}">Főoldal <span
                                            class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item
                                @if(Route::current()->getName() == 'faq')
                                        {{'active'}}
                                @endif
                                ">
                                    <a class="nav-link" href="{{route('faq')}}">Gy.i.k</a>
                                </li>
                                <li class="nav-item 
                                @if(Route::current()->parameters() == ['category_slug' => 'nemzetkozi-es-streetfood'])
                                        {{'active'}}
                                @endif
                                d-lg-none">
                                    <a class="nav-link" href="{{route('restaurants.byCategories', ['category_slug' => 'nemzetkozi-es-streetfood'])}}">Nemzetközi / Street food</a>
                                </li>
                                <li class="nav-item 
                                @if(Route::current()->parameters() == ['category_slug' => 'modern-fine-dining-ettermek'])
                                        {{'active'}}
                                @endif
                                d-lg-none">
                                    <a class="nav-link" href="{{route('restaurants.byCategories', ['category_slug' => 'modern-fine-dining-ettermek'])}}">Modern, fine dining</a>
                                </li>
                                <li class="nav-item 
                                @if(Route::current()->parameters() == ['category_slug' => 'magyaros-ettermek'])
                                        {{'active'}}
                                @endif
                                d-lg-none">
                                    <a class="nav-link" href="{{route('restaurants.byCategories', ['category_slug' => 'magyaros-ettermek'])}}">Magyaros éttermek</a>
                                </li>
                                <li class="nav-item 
                                @if(Route::current()->parameters() == ['category_slug' => 'cukraszda'])
                                        {{'active'}}
                                @endif
                                d-lg-none">
                                    <a class="nav-link" href="{{route('restaurants.byCategories', ['category_slug' => 'cukraszda'])}}">Cukrászda</a>
                                </li>
                                <li class="nav-item
                                @if(Route::current()->getName() == 'contact')
                                        {{'active'}}
                                @endif
                                ">
                                    <a class="nav-link" href="{{route('contact')}}">Kapcsolat</a>
                                </li>
                            </ul>
                            <!-- Search btn -->
                            <div class="dorne-search-btn">
                                <a id="search-btn" href="#"><i class="fa fa-search" aria-hidden="true"></i> Keresés</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>