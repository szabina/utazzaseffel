<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    /* $this->call(FeatureSeeder::class);
    $this->call(PermissionSeeder::class);
    $this->call(RoleSeeder::class);
    $this->call(AdminSeeder::class);
    $this->call(CategorySeeder::class);
    $this->call(CategoryDescription::class);
    $this->call(CategoriesSlugs::class); */
    $this->call(ChangeFeatureName::class);



  }
}
