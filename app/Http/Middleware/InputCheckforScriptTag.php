<?php

namespace App\Http\Middleware;

use Closure;

class InputCheckforScriptTag
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputs = $request->all();

        foreach ($inputs as $input) {
            if(gettype($input) == 'string' || gettype($input) == 'integer' || gettype($input) == 'boolean'){
            if (strpos($input, 'script') !== false) {
                return redirect()->back();
            }
        }
        }

        return $next($request);
    }
}
