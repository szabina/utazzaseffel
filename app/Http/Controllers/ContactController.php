<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use App\Models\Message;

class ContactController extends Controller
{
    public function contactForm(){
        return view('frontend.contact');
    }

    public function send(Request $request){
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'message' => $request->input('message')
        ];

        //üzenet mentése
        $message = new Message;
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->message = $request->input('message');
        $message->save();


        //Send email
        Mail::to('contact@contact.hu')->send(new ContactMail($data));
         
        return redirect()->route('contact')->with('status', 'Üzenetét sikeresen elküldte!');
    }
    
}
