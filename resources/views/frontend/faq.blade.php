<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<html lang="hu">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="Jakab László Szocska Szabina">
    <!-- Title -->
    <title>Utazz a séffel | GYIK</title>
    <script src="https://kit.fontawesome.com/9760bb6d35.js"></script>
    <link rel="stylesheet" href="realfontawesome.css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    <link rel="manifest" href="img/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Fontawsome -->


    <link href="css/random/allMin.css" rel="stylesheet">

    <link rel="stylesheet" href="custom.css">



</head>

<body
    style="background: url(img/gyikbg.jpg); background-attachment: fixed; background-position: center; background-size: cover">
    <!-- Preloader -->
    <div id="preloader">
        <div class="dorne-load"></div>
    </div>

    @include('frontend.navbar')
    <!-- ***** Header Area End ***** -->
    <!-- === END HEADER === -->
    <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image:  url({{asset('img/hero2.jpg')}})">
    </div>
    <!-- ***** Breadcumb Area End ***** -->
    <!-- === BEGIN CONTENT === -->
    <div class="container h-100  my-1 my-lg-3 accontainer">
        <div class="row-12 h-100 my-1 my-lg-2">
            <div class="col-12 ">
                <h2 class="heading-primary heading-primary-2">Gyakran ismételt kérdések</h2>
                <div class="accordion ">
                    <dl>
                        <dt>
                            <a href="#accordion1" aria-expanded="false" aria-controls="accordion1"
                                class="accordion-title accordionTitle js-accordionTrigger">Hogyan pontozunk?</a>
                        </dt>
                        <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                            <p>Egy-egy éttermet, cukrászdát több szempont szerint pontozunk:</p>
                            <ul class="faq-list">
                                <li class="faq-list-item">figyeljük az étterem hangulatát, tisztaságát, felszereltségét
                                    és hogy milyen egyéb szolgáltatásokkal rendelkezik.</li>
                                <li class="faq-list-item">megkóstolunk az étlapról több ételt: előételt, főételt,
                                    desszertet, majd azokat is pontozzuk összkép és ízvilág szerint, megnézzük, hogy
                                    a vendéglátó egység típusához illeszkedik-e a felszolgált étel.</li>
                                <li class="faq-list-item">az összbenyomásba beszámításra kerül még a kiszolgálás
                                    színvonala, a felszolgáló modora, rátermettsége, hozzáértése és gyorsasága is.</li>
                            </ul>
                            <p>Az összbenyomás úgy áll össze, hogy a megkóstolt ételek pontjai számítanak leginkább,
                                80%-ban, az étterem és a felszolgálás 10-10%-ban a végeredménybe.</p>
                        </dd>
                        <dt>
                            <a href="#accordion2" aria-expanded="false" aria-controls="accordion2"
                                class="accordion-title accordionTitle js-accordionTrigger">
                                Kik értékelnek?</a>
                        </dt>
                        <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                            <p>A fő értékelőnk 25 éve dolgozik a vendéglátás terén, melyből 15 évet
                                séfként tevékenykedett Magyarország több neves éttermében. Ő volt ennek az oldalnak az
                                ötletgazdája, főként az ő kifinomult ízlése alapján pontozzuk az ételeket,
                                de ellenpólusként kóstol még több „hétköznapi” személy is. Pontozóink kilétét nem fedjük
                                fel a hiteles kóstolás miatt.</p>
                        </dd>
                        <dt>
                            <a href="#accordion3" aria-expanded="false" aria-controls="accordion3"
                                class="accordion-title accordionTitle js-accordionTrigger">
                                Milyen sűrűn látogattok el egy vendéglátó egységbe?
                            </a>
                        </dt>
                        <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                            <p>Évente újraértékeljük a vendéglátó egységeket. Előre nem egyeztetett időpontban látogatunk
                                el hozzájuk. Pontozóink nem fedik fel kilétüket a hiteles kóstolás miatt.</p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>




    <!-- === END CONTENT === -->
    <!-- === BEGIN FOOTER === -->
    <!-- ****** Footer Area Start ****** -->
    <script src="{{asset('js/accordion.js')}}"></script>

    @include('frontend.footer')
</body>

</html>
<!-- === END FOOTER === -->