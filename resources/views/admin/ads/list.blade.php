 @extends('admin.layouts.template')

@section('content')
<h1>Hirdetések</h1>
@if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif 

@foreach ($ads as $ad)
    <div style="display:flex; flex-direction:row; justify-content: space-between; border-bottom: 1px solid grey; height: 150px; padding:10px">
        <img style="width: 200px;" src="/storage/restaurants/{{$ad->image}}">

        <div>Cím: {{$ad->title}}<br>
        URL: {{$ad->url}}<br>
        Alcím: {{$ad->sub_title}}<br></div>
        <div>
          <a style="margin-bottom:5px"  class="btn btn-success" href="{{route('ad.edit', ['ad_id' => $ad->id])}}">Módosítás</a>
          <form class="delete" action="{{route('ad.delete', ['ad_id' => $ad->id])}}" method="POST">
            @csrf
        
            <input type="hidden" name="_method" value="DELETE">        
            <button type="submit" class="btn btn-danger">Törlés</button>
          </form>
        
        </div>
          
    </div>
@endforeach

@endsection